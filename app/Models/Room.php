<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Room extends Model
{
    protected $table = 'rooms';

    protected $fillable = ['room_number', 'floor', 'slots', 'dorm_id'];

    public function dorm() {
        return $this->belongsTo(Dorm::class)->with('university');
    }

    public function users() {
        return $this->hasMany(OccupiedRoom::class, 'room_id', 'id');
    }

    /*public function occupants() {
        return $this->hasMany(OccupiedRoom::class, 'room_id', 'id');
    }*/

    public function occupants() {
        return $this->belongsToMany(User::class, 'occupied_rooms', 'room_id', 'user_id');
    }

    public function features() {
        return $this->belongsToMany(Feature::class, 'room_features', 'room_id', 'feature_id');
    }

    public static function getFreeSlots($room) {
        $roomSlots = self::select('slots')->where('id', $room)->get();
        $roomSlots = $roomSlots[0]->slots;
        $takenSlots = DB::table('occupied_rooms')->where('room_id', $room)->count();
        $freeSlots = $roomSlots - $takenSlots;

        return $freeSlots;
    }
}
