<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DormManager extends Model
{
    protected $table = 'dorm_managers';

    protected $fillable = ['user_id', 'dorm_id'];

    public function dorms() {
        return $this->hasMany(Dorm::class, 'id', 'dorm_id');
    }
}
