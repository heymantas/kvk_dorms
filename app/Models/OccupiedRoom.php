<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OccupiedRoom extends Model
{
    protected $table = 'occupied_rooms';

    protected $fillable = ['user_id', 'room_id', 'paid_until'];

    public function users() {
        return $this->hasMany(User::class,  'id', 'user_id');
    }

    public function room() {
        return $this->belongsTo(Room::class)->with('dorm')->with('occupants');
    }
}
