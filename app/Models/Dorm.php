<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Dorm extends Model
{
    protected $table = 'dorms';
    protected $fillable = ['name', 'address', 'lat', 'long', 'university_id'];

    public function university()
    {
        return $this->belongsTo(University::class, 'university_id', 'id')->with('city');
    }

    public function rooms()
    {
        return $this->hasMany(Room::class, 'dorm_id', 'id');
    }

    public function managers() {
        return $this->belongsToMany(User::class, 'dorm_managers', 'dorm_id', 'user_id');
    }
}
