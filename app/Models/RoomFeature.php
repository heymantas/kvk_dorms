<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RoomFeature extends Model
{
    protected $table = 'room_features';

    protected $fillable = ['room_id', 'feature_id'];

    public function features() {
        return $this->hasMany(Feature::class, 'id', 'feature_id');
    }

    public function rooms() {
        return $this->hasMany(Room::class);
    }
}
