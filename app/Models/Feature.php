<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Feature extends Model
{
    protected $table = 'features';

    protected $fillable = ['name'];

    public function rooms() {
        return $this->belongsToMany(RoomFeature::class, 'room_features', 'feature_id', 'id');
    }
}
