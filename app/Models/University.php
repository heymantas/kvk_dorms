<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class University extends Model
{
    protected $table = 'universities';

    protected $fillable = ['name', 'city'];

    public function city(){
        return $this->belongsTo(City::class);
    }

    public function dorms()
    {
        return $this->hasMany(Dorm::class, 'university_id', 'id');
    }
}
