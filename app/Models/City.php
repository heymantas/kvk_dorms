<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    public function universities(){
        return $this->hasMany(University::class);
    }
}
