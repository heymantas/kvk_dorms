<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RoomRequest extends Model
{
    protected $table = 'room_requests';

    protected $fillable = ['user_id', 'room_id'];

    public function users() {
        return $this->hasMany(User::class, 'id', 'user_id');
    }

    public function room() {
        return $this->belongsTo(Room::class)->with('dorm');
    }
}
