<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

use Illuminate\Support\Facades\Mail;
use App\Mail\AuthMail;


class User extends Authenticatable implements MustVerifyEmail
{
    use Notifiable;

    protected $table = 'users';

    protected $fillable = [
        'email', 'password','name','surname','phone','university_id', 'photo'
    ];

    protected $hidden = [
        'password', 'remember_token','api_token'
    ];

    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function roles() {
        return $this->belongsToMany(Role::class);
    }

    public function authorizeRoles($roles)
    {
        if ($this->hasAnyRole($roles)) {
            return true;
        }
        return false;
    }

    public function hasAnyRole($roles)
    {
        if (is_array($roles)) {
            foreach ($roles as $role) {
                if ($this->hasRole($role)) {
                    return true;
                }
            }
        } else {
            if ($this->hasRole($roles)) {
                return true;
            }
        }
        return false;
    }
    public function hasRole($role)
    {
        if ($this->roles()->where('name', $role)->first()) {
            return true;
        }
        else {
            return false;
        }
    }

    public function hasPermissionThroughRole($permission) {
        foreach ($permission->roles as $role){
           if($this->roles->contains($role)) {
              return true;
           }
        }
        return false;
     }

    public function university(){
        return $this->belongsTo(University::class);
    }

    public function dorms(){
        return $this->hasMany(Dorm::class,'manager_id');
    }

    public function room() {
        return $this->belongsTo(Room::class);
    }

    public function occupied_room() {
        $this->belongsTo(OccupiedRoom::class);
    }

    public function room_requests() {
        return $this->belongsToMany(RoomRequest::class, 'user_id', 'id');
    }
    /**
     * Send the password reset notification.
     *
     * @param  string  $token
     * @return void
     */
    public function sendPasswordResetNotification($token)
    {
        Mail::to($this)->queue(new AuthMail($this,"iš naujo nustatyti slaptažodį",'Nustatyti iš naujo',"reset",$token));
    }
}
