<?php

namespace App\Mail;

use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class PaymentWarning extends Mailable
{

    public $user;
    public $room;
    public $university;
    public $date;

    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @param User $user
     * @param $room
     * @param $university
     * @param $date
     */
    public function __construct(User $user, $room, $university, $date)
    {
        $this->user = $user;
        $this->room = $room;
        $this->university = $university;
        $this->date = $date;
    }


    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.payment-warning')->subject('Įspėjimas');
    }
}
