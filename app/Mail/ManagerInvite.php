<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

use Illuminate\Support\Facades\URL;
use Illuminate\Support\Carbon;

use Config;

class ManagerInvite extends Mailable
{
    use Queueable, SerializesModels;

    public $link;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($email)
    {
        $this->link = $this->generateVerificationLink($email);
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.manager-invite')->subject('Pakvietimas');
    }

    private function generateVerificationLink($email){
        return URL::temporarySignedRoute(
            'manager.register',
            Carbon::now()->addMinutes(60),
            ['email'=>$email]
        );
    }

}
