<?php

namespace App\Mail;

use App\Models\User;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

use Illuminate\Support\Facades\URL;
use Illuminate\Support\Carbon;

use Config;

class AuthMail extends Mailable
{
    use Queueable, SerializesModels;

    public $user;
    public $link;
    public $body;
    public $button;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $user, $body, $button,$reason = null, $token = null)
    {
        $this->user = $user;
        $this->body = $body;
        $this->button = $button;
        if($reason === 'email'){
            $this->link = $this->generateVerificationLink($this->user->id);
        }else if($reason === 'reset'){
            $this->link = $this->generateResetLink($token);
        }
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.authMail');
    }

    private function generateVerificationLink($id){
        return URL::temporarySignedRoute(
            'verification.verify',
            Carbon::now()->addMinutes(Config::get('auth.verification.expire', 60)),
            ['id' => $id]
        );
    }

    private function generateResetLink($token){
        $link = route('password.reset',['token'=>$token]);
        $link .= "?email=".$this->user->email;
        return $link;
    }
}
