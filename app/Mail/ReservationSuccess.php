<?php

namespace App\Mail;

use App\Models\Room;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ReservationSuccess extends Mailable
{
    use Queueable, SerializesModels;

    public $user;
    public $room;
    public $university;

    public function __construct(User $user, $room, $university)
    {
        $this->user = $user;
        $this->room = $room;
        $this->university = $university;
    }

    public function build()
    {
        return $this->markdown('emails.reservation-success')->subject('Rezervacija pateikta!');
    }
}
