<?php

namespace App\Http\Controllers;

use App\Models\RoleUser;
use App\Models\Role;
use App\Models\User;

use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Socialite;

class AuthController extends Controller
{
    /**
     * Create a new AuthController instance.
     *
     * @return void
     */

    public function SocialSignup()
    {
        try {
            $user = Socialite::driver('google')->stateless()->user();
        } catch (\Exception $e) {
            return $e;
        }
        // check if they're an existing user
        $existingUser = User::where('email', $user->email)->first();
        if($existingUser){
            // send token back
            Auth::login($existingUser);
            // $existingUser->roles;
            // return response(json_encode($existingUser),202);
            return response()->json(['success'=> true]);
            
        } else {
            // create a new user
            $newUser                    = new User;
            $newUser->name              = $user->user['given_name'] ?? '';
            $newUser->surname           = $user->user['family_name'] ?? '';
            $newUser->email             = $user->email;
            $newUser->google_id         = $user->id;
            $newUser->email_verified_at = time();

            // $newUser->save();

            return response(json_encode(['type'=>'partial','newUser'=>$newUser]));

        }
       // return redirect()->to('/');
    }
    public function SocialCallBack()
    {
        return view('index');
    }

    /**
     * Get the authenticated User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function me(Request $request)
    {
        $user = auth('api')->user();
        $user->roles;
        return response()->json($user,200);
    }

    public function refreshToken(Request $request)
    {
        $token = Str::random(60);
        $request->user()->forceFill([
            'api_token' => hash('sha256', $token),
        ])->save();

        return ['token' => $token];
    }
}