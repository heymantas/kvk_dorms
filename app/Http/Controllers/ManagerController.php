<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Carbon;

use Illuminate\Http\Request;
use App\Mail\ManagerInvite;

use App\Models\User;
use App\Models\RoleUser;
use App\Models\Invitations;

use Config;



class ManagerController extends Controller
{

    public function emailManager(Request $request){
        Mail::to($request->email)->queue(new ManagerInvite($request->email));
        return redirect('panel');
    }

    public function registerManager(Request $request){
        return view('auth.register_manager',['expires'=>$request->expires,'signature'=>$request->signature,'email'=>$request->email]);
    }

    public function store(Request $request){
        if($request->query('email') !== $request->email){
            abort(403);
        }
        $data = $request->validate([
            "name" => "required|string",
            "surname" => "required|string",
            "email" => "required|email|unique:users",
            "password" => 'required|min:8|string|confirmed',
        ]);

        $manager = new User;
        $manager->email = $data['email'];
        $manager->name = $data['name'];
        $manager->surname = $data['surname'];
        $manager->password = Hash::make($data['password']);
        $manager->save();

        $newRoleUser              = new RoleUser;
        $newRoleUser->user_id     = $manager->id;
        $newRoleUser->role_id     = 2;
        $newRoleUser->save();

        Auth::login($manager);

        return redirect('./panel');
    }
}
