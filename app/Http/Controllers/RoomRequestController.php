<?php

namespace App\Http\Controllers;

use App\Mail\ReservationSuccess;
use App\Models\City;
use App\Models\OccupiedRoom;
use App\Models\Room;
use App\Mail\ReservationDeclined;
use App\Models\RoomRequest;
use App\Models\User;
use App\Models\University;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

class RoomRequestController extends Controller
{
    public function getByUser($id)
    {
        $roomRequests = DB::table('room_requests')
            ->select('room_requests.user_id', 'rooms.id', 'rooms.room_number', 'rooms.photos', 'dorms.name AS dorm_name', 'universities.name AS uni_name', 'dorms.address')
            ->join('rooms', 'rooms.id', '=', 'room_requests.room_id')
            ->join('dorms', 'rooms.dorm_id', '=', 'dorms.id')
            ->join('universities', 'dorms.university_id', '=', 'universities.id')
            ->where('room_requests.user_id', $id)
            ->paginate(5);

        return response($roomRequests->jsonSerialize(), Response::HTTP_OK);
    }

    public function delete($user_id, $room_id)
    {
        $user = User::find($user_id);
        $room = Room::find($room_id);
        $exists = OccupiedRoom::where('user_id', $user_id)->first();

        RoomRequest::where('user_id', $user_id)->where('room_id', $room_id)->delete();
        if(!$exists) {
            Mail::to($user->email)->queue(new ReservationDeclined($user, $room));
        }
    }

    public function add(Request $request){
        $data = [];
        $user_id = $request->user('api')->id;
        $room_id = $request->room_id;
        $data['user_id'] = $user_id;
        $data['room_id'] = $room_id;
        $validator = Validator::make($data, [
            'user_id' => 'required|unique:room_requests',
            'room_id' => 'required'
        ]);
        if ($validator->fails()) {
            return response()->json([
                    'message' => $validator->errors(),
                    'success' => false
                ]
            );
        } else {
            RoomRequest::create($data);
            $user = Auth::user('api');
            $room_id = $request->room_id;
            $requestedRoom = Room::where('id', $room_id)->with('dorm')->get();
            $university_id = $requestedRoom[0]->dorm->university_id;
            $university = University::where('id', $university_id)->with('city')->get();
            Mail::to($user)->queue(new ReservationSuccess($user, $requestedRoom, $university));
            $message = 'Rezervacija pateikta';
            return response()->json([
                'message' => $message,
                'success' => true
            ]);
        }
    }

    public function all()
    {
        $roomRequests = DB::table('room_requests')
            ->select('room_requests.room_id', 'rooms.room_number', 'universities.name AS uni_name', 'dorms.name AS dorm_name', DB::raw('count(*) as total_r'))
            ->join('rooms', 'rooms.id', '=', 'room_requests.room_id')
            ->join('dorms', 'dorms.id', '=', 'rooms.dorm_id')
            ->join('universities', 'universities.id', '=', 'dorms.university_id')
            ->groupBy('room_requests.room_id')
            ->paginate(5);

        return response($roomRequests->jsonSerialize(), Response::HTTP_OK);
    }

    public function checkRequest($id)
    {
        $roomRequest = RoomRequest::where('user_id', $id)->get();
        if(sizeof($roomRequest) == 0) {
            return response()->json([
                'roomRequested' => false,
            ]);
        } else {
            return response()->json([
                'roomRequested' => true,
            ]);
        }
    }

    public function checkOccupation($id)
    {
        $roomOccupation = OccupiedRoom::where("user_id", $id)->get();
        if(sizeof($roomOccupation) == 0) {
            return response()->json([
                'hasRoom' => false,
            ]);
        } else {
            return response()->json([
                'hasRoom' => true,
            ]);
        }
    }


}
