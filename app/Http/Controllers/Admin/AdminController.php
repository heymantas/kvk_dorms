<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Support\Facades\Auth;
use App\Models\Room;
use App\Models\RoomRequest;
use App\Models\University;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Support\Facades\DB;


class AdminController extends Controller
{

    protected $pages = 7;

    public function index() {
        return redirect()->route('panel');
    }

    public function showUsers() {
        return view('panel/admin/pages/users');
    }

    public function managers(){
        return view('panel/admin/pages/managers');
    }

    public function inviteManager(){
        return view('panel/admin/pages/invite_manager');
    }

    public function showUser($id) {
        $user = DB::table('users')
            ->select('users.*', 'universities.name AS uni_name', 'universities.city_id as uni_city')
            ->join('universities', 'users.university_id', '=', 'universities.id')
            ->where('users.id', $id)
            ->get();

        $user = $user[0];

        $roomRequests = DB::table('room_requests')
            ->select('*')
            ->join('rooms', 'room_requests.room_id', '=', 'rooms.id')
            ->join('dorms', 'rooms.dorm_id', '=', 'dorms.id')
            ->where('room_requests.user_id', $id)
            ->get();

        return view('panel/admin/pages/user', compact('user', 'roomRequests'));
    }

    public function editUser(Request $request) {
        $user = User::with('university')->where('id',$request->id)->first();
        return view('panel/admin/pages/user_edit', compact('user'));
    }

    public function showPermissions() {
        return view('panel/admin/pages/permissions');
    }

    public function createPermissions(){
        return view('panel/admin/pages/permission_create');
    }

//    public function showRooms() {
//        return view('panel/admin/pages/rooms');
//    }
//
//    public function showRoomRequests() {
//        return view('panel/admin/pages/room_requests');
//    }
//
//    public function editRoom($room) {
//        return view('panel/admin/pages/room_edit');
//    }
//
//    public function createRoom() {
//        return view('panel/admin/pages/room_create');
//    }

    public function createUser() {
        return view('panel/admin/pages/user_create');
    }
}
