<?php

namespace App\Http\Controllers;

use App\Models\DormManager;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

use App\Models\RoomRequest;
use App\Models\Room;
use App\Models\Dorm;

class DormController extends Controller
{
    public function index()
    {
        $dorms = DB::select("SELECT universities.name as university_name, universities.city_id as city_id, dorms.*,
                                    SUM((rooms.slots - (SELECT COUNT(user_id) FROM
                                    occupied_rooms WHERE room_id = rooms.id))) as slots FROM `rooms`
                                    INNER JOIN dorms
                                    ON dorms.id = rooms.dorm_id
                                    LEFT JOIN universities ON dorms.university_id = universities.id
                                    GROUP BY dorms.id");
        return $dorms;
    }

    public function all()
    {
        $dorms = Dorm::all();
        return response($dorms->jsonSerialize(), Response::HTTP_OK);
    }

    public function getDorm($id)
    {
        $dorm = Dorm::find($id);
        return response($dorm->jsonSerialize(), Response::HTTP_OK);
    }

    public function allByUni($universityId)
    {
        $dorms = Dorm::where('university_id', $universityId)->get();

        return response($dorms->jsonSerialize(), Response::HTTP_OK);
    }

    public function createDorm($id)
    {
        return view('panel/admin/pages/dorm_create', ['university_id' => $id]);
    }

    public function addDormWithoutImg(Request $request)
    {
        $data = $request->validate([
            "name" => "required|string|unique:dorms",
            "address" => "required|unique:dorms",
            "university_id" => "required",
            "lat" => "required",
            "long" => "required",
            "manager_id" => "required",
        ]);

        $dorm = new Dorm;
        $dorm->name = $data['name'];
        $dorm->address = $data['address'];
        $dorm->lat = $data['lat'];
        $dorm->long = $data['long'];
        $dorm->university_id = $data['university_id'];
        $dorm->manager_id = $data['manager_id'];
        $dorm->photos = '[{"path":"\/images\/dorms\/default_dorm.png"}]';
        $dorm->save();

        $dormManager = new DormManager();
        $dormManager->user_id = $data['manager_id'];
        $dormId = Dorm::where('manager_id', $data['manager_id'])
            ->where('name', $data['name'])
            ->where('address', $data['address'])
            ->first()->id;
        $dormManager->dorm_id = $dormId;
        $dormManager->save();

        return response('created');

    }

    public function count()
    {
        $count = Dorm::count();
        return $count;
    }

    public function getUserDorm($id)
    {
        $dorms = Dorm::whereHas('managers', function ($q) use ($id) {
            $q->where('user_id', $id);
        })->with('managers')->get();

        return response()->json($dorms);
    }

    public function asignManager(Request $request)
    {

        $data = $request->validate([
            "id" => "required",
            "manager_id" => "required"
        ]);


        DormManager::where('dorm_id', $data['id'])->delete();
        Dorm::where('id', $data['id'])->update(['manager_id' => $data['manager_id']]);
        $dormManager = new DormManager();
        $dormManager->user_id = $data['manager_id'];
        $dormManager->dorm_id = $data['id'];
        $dormManager->save();
        return "success";
    }

    public function deleteAsignedManager($dorm_id)
    {
        Dorm::where('id', $dorm_id)->update(['manager_id' => null]);
        DormManager::where('dorm_id', $dorm_id)->delete();
        return 'deleted';
    }

    public function getDormOccupants($dormId)
    {
        $query = DB::table('occupied_rooms')
            ->select(
                'users.id', 'users.name', 'users.surname', 'users.email', 'users.gender', 'users.photo', 'users.phone',
                'occupied_rooms.room_id', 'occupied_rooms.paid_until',
                'rooms.room_number')
            ->join('users', 'users.id', '=', 'occupied_rooms.user_id')
            ->join('rooms', 'rooms.id', '=', 'occupied_rooms.room_id')
            ->join('dorms', 'dorms.id', '=', 'rooms.dorm_id')
            ->where('dorms.id', $dormId)
            ->orderBy('rooms.room_number')
            ->get();

        return response($query->jsonSerialize(), Response::HTTP_OK);
    }

    public function getDormFreeSlots($dorm)
    {
        $query = DB::table('rooms')
            ->select(DB::raw('SUM(slots) AS all_slots'))
            ->where('dorm_id', '=', $dorm)
            ->get();

        $query2 = DB::table('occupied_rooms')
            ->join('rooms', 'rooms.id', '=', 'occupied_rooms.room_id')
            ->join('dorms', 'dorms.id', '=', 'rooms.dorm_id')
            ->where('dorms.id', '=', $dorm)
            ->count();

        $count = $query[0]->all_slots - $query2;
        return $count;
    }

    public function getDormRoomRequests($dormId)
    {
        $data = RoomRequest::whereHas('room.dorm', function ($query) use ($dormId) {
            $query->where('id', $dormId);
        })
            ->with([
                'users' => function ($query) {
                    $query->select(['id', 'name', 'surname', 'email', 'university_id', 'gender', 'photo', 'phone']);
                },
                'room.dorm.university' => function ($query) {
                    $query->select('universities.id', 'universities.name');
                },
            ])->get();


        return $data;
    }

    public function showOneDorm(Request $request, $dorm_id, $gender, $orderBySlots)
    {
        $dorm = DB::select("SELECT universities.name as university_name, dorms.*,
                                    SUM((rooms.slots - (SELECT COUNT(user_id) FROM
                                    occupied_rooms WHERE room_id = rooms.id))) as slots, COUNT(rooms.id) as room_count FROM `rooms`
                                    INNER JOIN dorms
                                    ON dorms.id = rooms.dorm_id
                                    LEFT JOIN universities ON dorms.university_id = universities.id
                                    WHERE dorms.id = ?
                                    GROUP BY dorms.id", array($request->id));

        if ($gender == 1 or $gender == 2) {

            //return rooms and floors by gender

            $floors = Room::select('floor as floor_number')
                ->where('dorm_id', $dorm_id)->where('room_gender', $gender)->groupBy('floor')->get();
            $rooms = Room::where('dorm_id', $dorm_id)->where('room_gender', $gender)
                ->when($orderBySlots == "true", function ($query){
                    return $query->orderBy('slots', 'DESC');
                })->orderBy('room_number')->with('occupants')->paginate(6);
        } else {

            //dont validate by gender and return all
            $floors = Room::select('floor as floor_number')->where('dorm_id', $dorm_id)->groupBy('floor')->get();
            $rooms = Room::where('dorm_id', $dorm_id)->when($orderBySlots == "true", function ($query){
                return $query->orderBy('slots', 'DESC');
            })->orderBy('room_number')->with('occupants')->paginate(6);
        }

        return response()->json([
                'dorm' => $dorm,
                'floors' => $floors,
                'rooms' => $rooms,
            ]
        );
    }

    public function delete($id)
    {
        Dorm::where('id', $id)->delete();
    }

    public function updateMng(Request $request)
    {
        $photos = $request->photos;
        $destinationPath = public_path('/images/dorms/');

        $path_to_photo = array();

        foreach ($photos as $key => $photo) {
            if (isset($photo['name'])) {
                $name = uniqid($request->id . '_', false) . '.' . pathinfo($photo['name'], PATHINFO_EXTENSION);
                $image = file_get_contents($photo['path']);
                file_put_contents($destinationPath . '' . $name, $image);
                $path_to_photo[$key]['path'] = '/images/dorms/' . '' . $name;
            } else {
                $path_to_photo[$key]['path'] = $photo['path'];
            }
        }

        $path_to_photo = json_encode($path_to_photo);

        Dorm::where('id', $request->id)->update([
            'name' => $request->name,
            'photos' => $path_to_photo
        ]);

        return response()->json(['message' => 'Bendrabučio informacija atnaujinta']);
    }
}
