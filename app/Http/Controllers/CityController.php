<?php

namespace App\Http\Controllers;

use App\Models\City;
use Illuminate\Http\Request;

class CityController extends Controller
{
    public function all() {
        $cities = City::all();
        return $cities;
    }

    public function getCity($id) {
        $city = City::findOrFail($id);
        return $city;
    }
}
