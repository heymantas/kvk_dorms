<?php

namespace App\Http\Controllers;

use App\Models\Role;
use Illuminate\Http\Response;

class RoleController extends Controller
{
    public function all() {
        return response(Role::all()->jsonSerialize(), Response::HTTP_OK);
    }
}
