<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use App\Models\Permission;
use App\Models\Role;


class PermissionController extends Controller
{
    public function __construct(){
        $this->middleware('auth:web');
        $this->middleware('role:Administrator');
    }

    public function all() {
        $permissions = Permission::orderBy('name')->with('roles')->paginate(10);
        return response($permissions->jsonSerialize(), Response::HTTP_OK);
    }

    public function add(Request $request) {
        $exists = Permission::where('name', '=', $request->name)->first();
        if(!$exists) {
            $id = Permission::insertGetId([
                'name' => $request->name,
                'description' => $request->description,
            ]);
            foreach ($request->roles as $key => $value) {
                DB::table('permission_role')->insert([
                    'permission_id' => $id,
                    'role_id' => $value
                ]);
            }
            return 'Teisė sukurta';
        }
        else {
            return response('Teisė tokiu pavadinimu jau yra egzistuoja!',422);
        }
    }

    public function delete(Request $request){
        $response = Permission::where('id',$request->permission_id)->delete();
        return $response;
    }

    public function search(Request $request) {
        $result = Permission::with('roles')->where(DB::raw("name"), 'like', '%'. $request->keyword .'%')->orWhere(DB::raw("description"), 'like', '%'. $request->keyword .'%')->get();

        if(!$result) {
            $result = [];
        }

        return response($result->jsonSerialize(), Response::HTTP_OK);
    }
}
