<?php

namespace App\Http\Controllers;

use Illuminate\Http\Response;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Validation\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:web');
        //$this->middleware('role:Administrator');
    }

    public function all()
    {
        $users = User::with('roles')->orderBy('created_at', 'desc')->paginate(5);

        return response($users->jsonSerialize(), Response::HTTP_OK);
    }

    public function count()
    {
        $count = User::count();
        return $count;
    }

    public function allMng()
    {
        $users = User::select('*')->whereNotIn('id', function ($query) {
            $query->select('user_id')->from('occupied_rooms');
        })->get();
        return response()->json($users);
    }

    public function getManagers()
    {
        $managers = User::with('dorms')->whereHas('roles', function ($q) {
            $q->where('id', '=', '2');
        })->paginate(10);

        return response($managers->jsonSerialize(), Response::HTTP_OK);
    }

    public function searchManagers(Request $request)
    {
        $search = $request->keyword;
        $managers = User::with('dorms')
            ->whereHas('roles', function ($q) {
                $q->where('id', '=', '2');
            })->where(function ($q) use ($search) {
                $q->where(DB::raw("CONCAT(`name`, ' ', `surname`)"), 'like', '%' . $search . '%')
                    ->orWhere(DB::raw("CONCAT(`surname`, ' ', `name`)"), 'like', '%' . $search . '%')
                    ->orWhere('email', 'like', '%' . $search . '%');
            })->get();

        return response($managers->jsonSerialize(), Response::HTTP_OK);
    }

    public function search(Request $request)
    {
        $users = User::with('roles')->where(DB::raw("CONCAT(`name`, ' ', `surname`)"), 'like', '%' . $request->keyword . '%')->orWhere(DB::raw("CONCAT(`surname`, ' ', `name`)"), 'like', '%' . $request->keyword . '%')->get();
        return response($users->jsonSerialize(), Response::HTTP_OK);
    }

    public function getUser($id)
    {
        $users = User::select('users.*', 'role_user.role_id')
            ->join('role_user', 'role_user.user_id', '=', 'users.id')
            ->where('users.id', '=', $id)
            ->get();

        return response($users->jsonSerialize(), Response::HTTP_OK);
    }

    public function update(Request $request, $id)
    {
        $user = User::find($id);
        $user->update($request->all());

        DB::table('role_user')->where('user_id', '=', $id)->update(['role_id' => $request->role]);
    }

    public function delete($id)
    {
        User::where('id', $id)->delete();
    }

    public function add(Request $request)
    {
        $exists = User::where('email', '=', $request->email)->first();
        if (!$exists) {
            $password = Hash::make($request->password);

            $newUser = new User;
            $newUser->name = $request->name;
            $newUser->surname = $request->surname;
            $newUser->phone = $request->phone;
            $newUser->email = $request->email;
            $newUser->password = $password;
            $newUser->university_id = $request->university_id;
            $newUser->save();

            DB::table('role_user')->insert([
                'user_id' => $newUser->id,
                'role_id' => $request->role_id
            ]);

            return 'Vartotojas sukurtas';
        } else {
            return 'Toks el. paštas jau yra užregistruotas';
        }
    }
}
