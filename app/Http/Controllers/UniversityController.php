<?php

namespace App\Http\Controllers;

use App\Models\University;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;

class UniversityController extends Controller
{

    public function index(){
        return view('panel/admin/pages/universities');
    }

    public function all() {
        return response(University::with('city','dorms')->orderBy('created_at','DESC')->paginate(10)->jsonSerialize(), Response::HTTP_OK);
    }

    public function getCity(Request $request){
        $city = University::where('id',$request->id)->first()->city;
        return $city;
    }

    public function list(){
        $list = University::with('city')->orderBy('name')->get();
        return $list;
    }

    public function count(){
        $count = University::count();
        return $count;
    }

    public function createUniversity(){
        return view('panel/admin/pages/university_create');
    }

    public function add(Request $request){
        $data = $request->validate([
            "name" => "required|string|unique:universities",
            "city_id" => "required",
        ]);

        $university = new University;
        $university->name = $data['name'];
        $university->city_id = $data['city_id'];
        $university->save();
        return response('created');
    }

    public function update(Request $request){
        $university = University::find($request->id);
        $university->name = $request->name;
        $university->city_id = $request->city_id;
        return response((string)$university->save());
    }

    public function delete(Request $request){
        University::find($request->id)->delete();
    }

    public function search(Request $request) {
        $universities = University::with('city','dorms')->where(DB::raw("`name`"), 'like', '%'. $request->keyword .'%')->get();
        return response($universities->jsonSerialize(), Response::HTTP_OK);
    }

    public function getUniversity($id) {
        $university = University::findOrFail($id);
        return $university;
    }
}
