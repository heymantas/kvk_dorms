<?php

namespace App\Http\Controllers;

use App\Models\Room;
use App\Models\RoomFeature;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;


class RoomController extends Controller
{
    public function all()
    {
        $rooms = Room::select('rooms.id AS room_id', 'rooms.room_number', 'rooms.floor', 'rooms.slots', 'rooms.photos', 'dorms.name AS dorm_name', 'universities.name AS uni_name', 'universities.id as uni_id')
            ->join('dorms', 'dorms.id', '=', 'rooms.dorm_id')
            ->join('universities', 'universities.id', '=', 'dorms.university_id')
            ->paginate(8);

        return response($rooms->jsonSerialize(), Response::HTTP_OK);
    }

    public function getRoom($dorm_id, $room_id)
    {
        $room = Room::where('dorm_id', $dorm_id)->where('id', $room_id)->with('dorm')->with('occupants')->get();
        return $room;
    }

    public function get($roomId)
    {
        $room = Room::where('id', $roomId)->with(['dorm', 'occupants', 'features'])->first();
        return response()->json($room);
    }

    public function searchByDorm($id)
    {
        $rooms = Room::select('rooms.id AS room_id', 'rooms.room_number', 'rooms.floor', 'rooms.slots', 'rooms.photos', 'dorms.name AS dorm_name', 'universities.name AS uni_name', 'universities.id as uni_id')
            ->join('dorms', 'dorms.id', '=', 'rooms.dorm_id')
            ->join('universities', 'universities.id', '=', 'dorms.university_id')
            ->where('dorms.id', '=', $id)
            ->paginate(8);
    }

    public function count()
    {
        $count = Room::count();
        return $count;
    }

    public function getDormRooms($dormId)
    {
        $rooms = Room::whereHas('dorm', function ($q) use ($dormId) {
            $q->where('dorm_id', '=', $dormId);
        })->with(['dorm', 'occupants', 'features'])->orderBy('room_number')->get();

        return response()->json($rooms);
    }

    /*    public function getDormRooms($managerId)
        {
            $rooms = Room::whereHas('dorm', function($q) use ($dormId) {
     $q->where('dorm_id', '=', $dormId);
     })->with(['dorm', 'occupants', 'features'])->get();

            $data = [];

            foreach ($rooms as $room) {
                if ($room->dorm->manager_id == $managerId) {
                    $data[$room->id] = array(
                        "id" => $room->id,
                        "room_number" => $room->room_number,
                        "floor" => $room->floor,
                        "slots" => $room->slots,
                        "price" => $room->price,
                        "features" => $room->features,
                        "photos" => $room->photos,
                        "dorm" => array($room->dorm)
                    );

                    foreach ($room->occupants as $user) {
                        $data[$room->id]['users'][] = $user->users;
                    }

                    return response()->json($rooms);
                }
            }
        }*/

    public function update(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'required',
            'room_number' => 'required',
            'floor' => 'required',
            'slots' => 'required',
            'dorm_id' => 'required',
            'price' => 'required',
            'features' => 'array',
            'photos' => 'array'
        ]);

        if ($validator->fails()) {
            return response()->json(['message' => 'Validation failed.', 'success' => 0]);
        }

        $exists = Room::where('id', '!=', $request->id)->where([
            ['dorm_id', $request->dorm_id],
            ['room_number', $request->room_number]
        ])->count();

        if ($exists) {
            $message = 'Kambarys tokiu numeriu jau užregistruotas.';
            return response()->json(['message' => $message, 'success' => 0]);
        }

        $photos = $request->photos;
        $destinationPath = public_path('/images/rooms/');

        $path_to_photo = array();

        foreach ($photos as $key => $photo) {
            if (isset($photo['name'])) {
                $name = uniqid($request->room_number . '_' . $request->dorm_id . '_', false) . '.' . pathinfo($photo['name'], PATHINFO_EXTENSION);
                $image = file_get_contents($photo['path']);
                file_put_contents($destinationPath . '' . $name, $image);
                $path_to_photo[$key]['path'] = '/images/rooms/' . '' . $name;
            } else {
                $path_to_photo[$key]['path'] = $photo['path'];
            }
        }

        $path_to_photo = json_encode($path_to_photo);

        Room::where('id', $request->id)
            ->update([
                'room_number' => $request->room_number,
                'floor' => $request->floor,
                'slots' => $request->slots,
                'dorm_id' => $request->dorm_id,
                'price' => $request->price,
                'photos' => $path_to_photo
            ]);

        RoomFeature::where('room_id', $request->id)->delete();

        foreach ($request->features as $feature) {
            RoomFeature::insert([
                'room_id' => $request->id,
                'feature_id' => $feature['value']
            ]);
        }

        $message = 'Kambario informacija atnaujinta.';
        return response()->json(['message' => $message, 'success' => 1]);
    }

    public function searchByFloor($dorm_id, $floor, $gender, $orderBySlots)
    {

        if ($gender == 2 || $gender == 1) {
            $rooms = Room::where('floor', $floor)
                ->where('dorm_id', $dorm_id)
                ->where('room_gender', $gender)
                ->when($orderBySlots == "true", function ($query) {
                    return $query->orderBy('slots', 'DESC');
                })
                ->orderBy('room_number')->with('occupants')->paginate(6);
        } else {
            $rooms = Room::where('floor', $floor)
                ->where('dorm_id', $dorm_id)
                ->when($orderBySlots == "true", function ($query) {
                    return $query->orderBy('slots', 'DESC');
                })
                ->orderBy('room_number')->with('occupants')->paginate(6);
        }

        return $rooms;
    }

    public function add(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'room_number' => 'required',
            'floor' => 'required',
            'slots' => 'required',
            'dorm_id' => 'required',
            'price' => 'required',
            'gender' => 'required',
            'features' => 'array',
            'photos' => 'array'
        ]);

        if ($validator->fails()) {
            return response()->json(['message' => 'Validation failed.']);
        }

        $exists = Room::where([
            ['room_number', $request->room_number],
            ['dorm_id', $request->dorm_id]
        ])->count();

        if ($exists) {
            $message = 'Kambarys tokiu numeriu bendrabutyje jau užregistruotas';
            return response()->json(['message' => $message]);
        }

        $photos = $request->photos;
        $destinationPath = public_path('/images/rooms/');

        $path_to_photo = array();

        if (sizeof($path_to_photo) > 0) {
            foreach ($photos as $key => $photo) {
                $name = uniqid($request->room_number . '_' . $request->dorm_id . '_', false) . '.' . pathinfo($photo['name'], PATHINFO_EXTENSION);
                $image = file_get_contents($photo['path']);
                file_put_contents($destinationPath . '' . $name, $image);
                $path_to_photo[$key]['path'] = '/images/rooms/' . '' . $name;
            }
        } else {
            $path_to_photo[0]['path'] = '/images/rooms/default_room.png';
        }

        $path_to_photo = json_encode($path_to_photo);

        $id = DB::table('rooms')->insertGetId([
            'room_number' => $request->room_number,
            'floor' => $request->floor,
            'slots' => $request->slots,
            'dorm_id' => $request->dorm_id,
            'price' => $request->price,
            'room_gender' => $request->gender,
            'description' => $request->description,
            'photos' => $path_to_photo
        ]);

        foreach ($request->features as $feature) {
            RoomFeature::insert([
                'room_id' => $id,
                'feature_id' => $feature['value']
            ]);
        }

        $message = 'Kambarys sukurtas';
        return response()->json(['message' => $message]);
    }

    public function delete($roomId)
    {
        Room::where('id', $roomId)->delete();

        return response()->json(['message' => 'Kambarys ištrintas']);
    }
}
