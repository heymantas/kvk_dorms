<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Feature;

class FeatureController extends Controller
{
    public function all() {
        $features = Feature::all();

        return response()->json($features);
    }
}
