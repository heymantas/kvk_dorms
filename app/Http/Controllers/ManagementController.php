<?php

namespace App\Http\Controllers;

use App\Models\Dorm;
use App\Models\DormManager;
use Illuminate\Support\Facades\Auth;
class ManagementController extends Controller
{
    public function hasDorm() {
        $hasDorm = DormManager::where('user_id', Auth::user()->id)->first();
        return $hasDorm;
    }

    public function registerDorm() {
        return view('panel/management/pages/dorm_register');
    }

    public function index() {
        if(!$this->hasDorm()) {
            return redirect()->route('management.dorm.register');
        }
        return redirect()->route('panel');
    }
}
