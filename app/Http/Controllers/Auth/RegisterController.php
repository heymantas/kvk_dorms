<?php

namespace App\Http\Controllers\Auth;

use App\Models\User;
use App\Models\RoleUser;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use Illuminate\Support\Facades\Mail;
use App\Mail\AuthMail;


class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */


    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'email' => ['required', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'min:8', 'string', 'confirmed'],
            'name' => ['required'],
            'surname' => ['required'],
            'university_id' => ['required'],
            'phone' => ['required'],
            'gender' => ['required'],
            'google_id' => ['sometimes', 'required'],
            'email_verified_at' => ['sometimes']
        ]);
    }

    public function register(Request $request)
    {

        $data = $this->validator($request->all())->validate();
        $newUser = new User;
        $newUser->name = $data['name'];
        $newUser->surname = $data['surname'];
        $newUser->email = $data['email'];
        $newUser->gender = $data['gender'];
        $newUser->google_id = $data['google_id'] ?? null;
        $newUser->email_verified_at = $data['email_verified_at'] ?? null;
        $newUser->phone = $data['phone'];
        $newUser->password = Hash::make($data['password']);
        $newUser->university_id = $data['university_id'];

        $newUser->save();

        $newRoleUser = new RoleUser;
        $newRoleUser->user_id = $newUser->id;
        $newRoleUser->save();

        if (!isset($data['email_verified_at'])) {
            $body = "Patvirtinti el-paštą";
            $button = "Patvirtinti";
            Mail::to($newUser)->queue(new AuthMail($newUser, $body, $button, 'email'));
        }

        Auth::login($newUser);

        return response()->json(['success' => true]);
    }
}
