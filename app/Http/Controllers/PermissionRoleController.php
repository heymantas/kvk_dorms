<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\PermissionRole;

class PermissionRoleController extends Controller
{
    public function __construct(){
        $this->middleware('auth:web');
        $this->middleware('role:Administrator');
    }

    public function add(Request $request)
    {
        $permissionRole = new PermissionRole;
        $permissionRole->permission_id = $request->permission_id;
        $permissionRole->role_id = $request->role_id;
        $permissionRole->save();

        //return response((string)$permissionRole->save(),200);
    }

    public function delete(Request $request)
    {
        $record = PermissionRole::where('role_id',$request->role_id)->where('permission_id',$request->permission_id)->delete();

        return response((string)$record,200);
    }
}
