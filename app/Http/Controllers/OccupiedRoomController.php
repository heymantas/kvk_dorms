<?php

namespace App\Http\Controllers;

use App\Mail\PaymentWarning;
use App\Mail\RoomChange;
use App\Models\RoomRequest;
use App\Models\University;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\OccupiedRoom;
use App\Models\Room;
use App\Models\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Mail;
use App\Mail\ReservationAccepted;

class OccupiedRoomController extends Controller
{
    public function changeRoom(Request $request) {
        OccupiedRoom::where('user_id', $request->user_id)->update(['room_id' => $request->room_id]);

        $user = User::find($request->user_id);
        $room = Room::find($request->room_id);

        Mail::to($user->email)->queue(new RoomChange($user, $room));

        return response()->json(['message' => 'Nuomininkas perkeltas']);
    }
    public function add(Request $request) {
        $exists = OccupiedRoom::where('user_id', $request->user)->count();

        $hasSlot = Room::getFreeSlots($request->room);

        if($exists) {
            $message = 'Vartotojas jau yra apsistojęs kambaryje';
            $added = false;
        }
        else if(!$hasSlot) {
            $message = 'Kambaryje nėra laisvų vietų';
            $added = false;
        }

        else {
            $user = User::find($request->user);
            $room = Room::find($request->room);
            $data['email'] = $user->email;
            DB::table('occupied_rooms')->insert(
                ['user_id' => $request->user, 'room_id' => $request->room]
            );
            $message = 'Rezervacija patvirtinta';
            $added = true;

            Mail::to($user->email)->queue(new ReservationAccepted($user, $room));
        }

        return response()->json(['message' => $message, 'added' => $added]);
    }

//    public function all() {
//        $rooms = Room::with('occupants')->get();
//        return response()->json($rooms);
//    }

    public function delete($user_id, $room_id){
        OccupiedRoom::where('user_id', $user_id)->where('room_id', $room_id)->delete();

        $message = 'Nuomininkas pašalintas';
        return response()->json(['message' => $message]);
    }

    public function updatePaymentDate(Request $request)  {

        $data = $request->validate([
            "user_id" => "required|integer",
            "room_id" => "required|integer",
            "paid_until" => "required|date"
        ]);

        OccupiedRoom::where('user_id', $data['user_id'])
            ->where('room_id', $data['room_id'])
            ->update(['paid_until' => $data['paid_until']]);

        return response()->json(['message' => 'Atnaujinta!']);
    }

    public  function sendPaymentWarning(Request $request) {

        $user = User::find($request->user_id);
        $room = Room::where('id', $request->room_id)->with('dorm')->first();

        $university_id = $room->dorm->university_id;
        $university = University::where('id', $university_id)->with('city')->first();

        $date = DB::table('occupied_rooms')
            ->select('paid_until')
            ->where('user_id', $request->user_id)
            ->first();

        if($date->paid_until) {
            Mail::to($user->email)->queue(new PaymentWarning($user, $room, $university, $date));
        } else {
            $date->paid_until = "nėra";
            Mail::to($user->email)->queue(new PaymentWarning($user, $room, $university, $date));
        }
        return response()->json([
            'sent_message' => "Laiškas išsiųstas.",
            'date' => $date
        ]);
    }

}
