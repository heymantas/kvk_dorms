<?php

namespace App\Http\Controllers;

use App\Mail\AuthMail;
use App\Models\OccupiedRoom;
use App\Models\Room;
use App\Models\RoomRequest;
use App\Models\User;
use function GuzzleHttp\Psr7\str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\File;

class UserPanelController extends Controller
{
    public function getUserOccupiedRoom($user_id)
    {
        $room = OccupiedRoom::where("user_id", $user_id)->with('users')->with('room')->first();
        return $room;
    }

    public function getRequest($user_id)
    {
        $roomRequest = RoomRequest::where('user_id', $user_id)->with('room')->first();
        return $roomRequest;
    }

    public function deleteRequest($user_id)
    {
        RoomRequest::where('user_id', $user_id)->delete();
    }

    public function getRoommates($room_id)
    {
        $roommates = OccupiedRoom::where('room_id', $room_id)->with('users')->get();
        return $roommates;
    }

    public function updateUserInfo(Request $request, $id)
    {

        $this->validate($request, [
            'name' => 'required|string|max:40',
            'surname' => 'required|string|max:50',
            'phone' => 'required|string|max:15',
            'university_id' => 'required|integer',
        ]);

        $user = User::findOrFail($id);

        //delete current user photo
        $image_path = public_path(). "/images/users/" . $user->photo;
        if(File::exists($image_path)) {
            File::delete($image_path);
        }

        $image_path = null;
        $name = null;

        //create new profile pic

        if ($request->get('photo')) {
            $image = $request->get('photo');
            $name = time() . '.' . explode('/', explode(':', substr($image, 0, strpos($image, ';')))[1])[1];
            Image::make($request->get('photo'))->save(public_path('images/users/') . $name);
            $user->photo = $name;
        } else {
            $user->photo = null;
        }

        $user->name = $request->name;
        $user->surname = $request->surname;
        $user->phone = $request->phone;
        $user->university_id = $request->university_id;
        $user->save();
        return $user;
    }

    public function updateEmail(Request $request, $id)
    {
        $this->validate($request, [
            'email' => 'required|email|max:255|unique:users'
        ]);

        $user = User::findOrFail($id);
        $user->email = $request->email;
        $user->email_verified_at = NULL;
        $user->save();

        //Send email
        $body = "Patvirtinti el-paštą";
        $button = "Patvirtinti";
        Mail::to($user)->queue(new AuthMail($user, $body, $button, 'email'));
        return $user;
    }

    public function deleteUser($id)
    {
        User::where('id', $id)->delete();
    }

}
