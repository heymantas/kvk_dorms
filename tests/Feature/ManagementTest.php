<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\URL;
use mysql_xdevapi\Table;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ManagementTest extends TestCase
{
    use DatabaseTransactions;

    /** @test */
    public function can_access_management_page()
    {
        $managerUser = factory(User::class)->create();
        DB::table('role_user')->insert([
            [
                'user_id' => $managerUser->id,
                'role_id' => 2
            ]
        ]);

        $this->actingAs($managerUser, 'web');
        $response = $this->get('/management');

        //manager doesn't have a dorm, redirecting to register page
        $response->assertRedirect('/management/register');

        DB::table('dorm_managers')->insert([
            'user_id' => $managerUser->id,
            'dorm_id' => 1
        ]);

        //manager has a dorm
        $response = $this->get('/management');
        $response->assertRedirect('/panel');

        $userWithoutPermission = factory(User::class)->create();
        DB::table('role_user')->insert([
            [
                'user_id' => $userWithoutPermission->id,
                'role_id' => 3
            ]
        ]);

        //user does not have a manager role
        $this->actingAs($userWithoutPermission, 'web');
        $response = $this->get('/management');
        $response->assertUnauthorized();
    }

    /** @test */
    public function can_access_dorm_register_page()
    {
        $managerUser = factory(User::class)->create();

        DB::table('role_user')->insert([
            [
                'user_id' => $managerUser->id,
                'role_id' => 2
            ]
        ]);

        $this->actingAs($managerUser, 'web');
        $response = $this->get('/management/register');
        $response->assertStatus(200);

    }

    public function test_invited_manager_can_access_invite_page()
    {
        $link = $this->generateVerificationLink("vartotojas@testavimas.lt");
        $response = $this->get($link);
        $response->assertStatus(200);
    }

    public function test_invited_manager_can_register()
    {
        $link = $this->generateVerificationLink("vartotojas@testavimas.lt");
        $response = $this->post($link, [
            "name" => "John",
            "surname" => "Johnson",
            "email" => "vartotojas@testavimas.lt",
            'password' => "123456789",
            "password_confirmation" => "123456789",
        ]);

        $response->assertRedirect('./panel');

        //oops, email doesnt match with link
        $response = $this->post($link, [
            "name" => "John",
            "surname" => "Johnson",
            "email" => " ",
            'password' => "123456789",
            "password_confirmation" => "123456789",
        ]);

        $response->assertStatus(403);



    }

    private function generateVerificationLink($email){
        return URL::temporarySignedRoute(
            'manager.register',
            Carbon::now()->addMinutes(60),
            ['email'=>$email]
        );
    }
}
