<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\DB;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class AdminTest extends TestCase
{

    use DatabaseTransactions;

    public function test_admin_can_visit_invite_page()
    {
        $this->createAdminUser();
        $response = $this->get('/admin/manager/invite');
        $response->assertStatus(200);
    }

    public function test_admin_can_invite_manager()
    {
        $this->createAdminUser();
        $response = $this->post('/admin/manager/email/send', [
            'email' => "manager123@test.lt"
        ]);
        $response->assertRedirect('/panel');
    }

    public function test_admin_can_access_panel()
    {
        $this->createAdminUser();
        $response = $this->get('/admin');
        $response->assertRedirect('/panel');
    }

    public function test_admin_can_access_users_page()
    {
        $this->createAdminUser();
        $response = $this->get('/admin/users');
        $response->assertStatus(200);
    }

    public function test_admin_can_view_user()
    {
        $this->createAdminUser();
        $user = factory(User::class)->create();
        $response = $this->get('/admin/user/view/' . $user->id);
        $response->assertStatus(200);
    }

    public function test_admin_can_access_user_edit_page()
    {
        $this->createAdminUser();
        $user = factory(User::class)->create();
        $response = $this->get('/admin/user/edit/' . $user->id);
        $response->assertStatus(200);
    }

    public function test_admin_can_access_user_create_page()
    {
        $this->createAdminUser();
        $response = $this->get('/admin/user/create');
        $response->assertStatus(200);
    }

    public function test_admin_can_access_managers_page()
    {
        $this->createAdminUser();
        $response = $this->get('/admin/managers');
        $response->assertStatus(200);
    }

    public function test_admin_can_access_permissions_page()
    {
        $this->createAdminUser();
        $response = $this->get('/admin/permissions');
        $response->assertStatus(200);
    }

    public function test_admin_can_access_permission_create_page()
    {
        $this->createAdminUser();
        $response = $this->get('/admin/permissions/create');
        $response->assertStatus(200);
    }

    private function createAdminUser()
    {
        $adminUser = factory(User::class)->create();
        DB::table('role_user')->insert([
            [
                'user_id' => $adminUser->id,
                'role_id' => 1
            ]
        ]);

        $this->actingAs($adminUser, 'web');
    }
}
