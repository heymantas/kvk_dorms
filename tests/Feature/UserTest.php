<?php

namespace Tests\Feature;

use App\Models\User;
use Faker\Factory as Faker;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\DB;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UserTest extends TestCase
{
    use DatabaseTransactions;

    public function test_admin_get_user()
    {
        $this->createAdminUser();
        $user = factory(User::class)->create();
        $response = $this->get('/admin/user/get/' . $user->id);
        $response->assertStatus(200);
    }

    public function test_admin_get_all_users()
    {
        $this->createAdminUser();
        $response = $this->get('/admin/user/all');
        $response->assertStatus(200);
    }

    public function test_admin_get_all_managers()
    {
        $this->createAdminUser();
        $response = $this->get('/admin/user/managers');
        $response->assertStatus(200);
    }

    public function test_user_search()
    {
        $this->createAdminUser();
        $response = $this->get('/admin/user/search');
        $response->assertStatus(200);
    }

    public function test_admin_can_delete_user()
    {
        $this->createAdminUser();
        $user = factory(User::class)->create();
        $response = $this->delete('/admin/user/delete/' . $user->id);
        $response->assertStatus(200);
    }

    public function test_admin_can_create_user()
    {
        $this->createAdminUser();
        $faker = Faker::create();
        $userArray = [
            'name' => $faker->firstName,
            'surname' => $faker->lastName,
            'phone' => $faker->phoneNumber,
            'email' => $faker->unique()->email,
            'password' => $faker->password,
            'university_id' => 1,
            'role_id' => 3
        ];

        $response = $this->post('/admin/user/add', $userArray);
        $response->assertStatus(200);

        $userWithSameEmail = [
            'name' => $faker->firstName,
            'surname' => $faker->lastName,
            'phone' => $faker->phoneNumber,
            'email' => $userArray['email'],
            'password' => $faker->password,
            'university_id' => 1,
            'role_id' => 3
        ];

        $response = $this->post('/admin/user/add', $userWithSameEmail);
        $response->assertStatus(200);
    }

    public function test_admin_search_managers()
    {
        $this->createAdminUser();
        $response = $this->get('/admin/managers/search', [
            'keyword' => "test"
        ]);

        $response->assertStatus(200);
    }

    public function test_admin_get_user_count()
    {
        $this->createAdminUser();
        $response = $this->get('/admin/user/count');
        $response->assertStatus(200);
    }

    public function test_admin_can_edit_user()
    {
        $this->createAdminUser();
        $user = factory(User::class)->create();
        $response = $this->put('/admin/user/update/' . $user->id, [
            'name' => "Changed name",
            'surname' => $user->surname,
            'phone' => $user->phone,
            'email' => $user->email,
            'university_id' => $user->university_id,
            'role' => 3
        ]);

        $response->assertStatus(200);
    }

    public function test_all_users_without_occupation()
    {
        $this->createManagerUser();
        $response = $this->get('/management/user/free/all');
        $response->assertStatus(200);
    }

    private function createAdminUser()
    {
        $adminUser = factory(User::class)->create();
        DB::table('role_user')->insert([
            [
                'user_id' => $adminUser->id,
                'role_id' => 1
            ]
        ]);

        $this->actingAs($adminUser, 'web');
    }

    private function createManagerUser()
    {
        $managerUser = factory(User::class)->create();
        DB::table('role_user')->insert([
            [
                'user_id' => $managerUser->id,
                'role_id' => 2
            ]
        ]);

        $this->actingAs($managerUser, 'web');
        return $managerUser;
    }
}
