<?php

namespace Tests\Feature;

use App\Models\Permission;
use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\DB;
use Tests\TestCase;

class PermissionTest extends TestCase
{
    use DatabaseTransactions;

    public function test_admin_get_all_permissions()
    {
        $this->createAdminUser();
        $response = $this->get('/admin/permissions/all');
        $response->assertStatus(200);
    }

    public function test_admin_can_search_permissions()
    {
        $this->createAdminUser();
        $response = $this->get('/admin/permissions/search', [
            'keyword' => 'dorm',
        ]);
        $response->assertStatus(200);
    }

    public function test_admin_can_add_permission()
    {
        $this->createAdminUser();
        $response = $this->post('/admin/permissions/add', [
            "name" => "permission-create",
            "description" => "Leidimu kurimas",
            'roles' => [1]
        ]);

        $response->assertStatus(200);

        //duplicate not allowed - test
        $response = $this->post('/admin/permissions/add', [
            "name" => "permission-create",
            "description" => "Leidimu kurimas",
            'roles' => []
        ]);

        $response->assertStatus(422);
    }

    public function test_admin_can_delete_permission()
    {
        $this->createAdminUser();
        $permission = factory(Permission::class)->create();
        $response = $this->delete('/admin/permissions/delete/' . $permission->id);
        $response->assertStatus(200);
    }

    private function createAdminUser()
    {
        $adminUser = factory(User::class)->create();
        DB::table('role_user')->insert([
            [
                'user_id' => $adminUser->id,
                'role_id' => 1
            ]
        ]);

        $this->actingAs($adminUser, 'web');
    }
}
