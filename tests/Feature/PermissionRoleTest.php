<?php

namespace Tests\Feature;

use App\Models\Permission;
use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\DB;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class PermissionRoleTest extends TestCase
{
    use DatabaseTransactions;

    public function test_admin_can_add_permission_to_role()
    {
        $this->createAdminUser();
        $permission = factory(Permission::class)->create();

        //add
        $response = $this->post('/admin/permissionRole/add', [
            "permission_id" => $permission->id,
            "role_id" => 1
        ]);

        $response->assertStatus(200);

        //delete
        $response = $this->delete('/admin/permissionRole/delete/1&'. $permission->id);
        $response->assertStatus(200);


    }

    private function createAdminUser()
    {
        $adminUser = factory(User::class)->create();
        DB::table('role_user')->insert([
            [
                'user_id' => $adminUser->id,
                'role_id' => 1
            ]
        ]);

        $this->actingAs($adminUser, 'web');
    }
}
