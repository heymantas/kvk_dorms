<?php

namespace Tests\Feature;

use App\Models\Dorm;
use App\Models\Room;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\DB;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class OccupiedRoomTest extends TestCase
{

    use DatabaseTransactions;

    public function test_manager_can_change_room()
    {
        $managerUser = $this->createManagerUser();
        $user = factory(User::class)->create();
        $dorm = factory(Dorm::class)->create();
        $room = factory(Room::class)->create([
            'dorm_id' => $dorm->id
        ]);

        $response = $this->put('/management/oc-room/update', [
            'user_id' => $user->id,
            'room_id' => $room->id
        ]);

        $response->assertStatus(200);
    }

    public function test_manager_can_update_payment_date()
    {
        $managerUser = $this->createManagerUser();
        $user = factory(User::class)->create();
        $user = factory(User::class)->create();
        $dorm = factory(Dorm::class)->create();
        $room = factory(Room::class)->create([
            'dorm_id' => $dorm->id
        ]);

        DB::table('occupied_rooms')->insert([
            [
                'user_id' => $user->id,
                'room_id' => $room->id,
                'paid_until' => Carbon::now()
            ]
        ]);

        $response = $this->put('/management/oc-room/update-payment-date', [
            'user_id' => $user->id,
            'room_id' => $room->id,
            'paid_until' => Carbon::now()->format('Y-m-d')
        ]);

        $response->assertStatus(200);
    }

    public function test_manager_can_send_payment_warning()
    {
        $managerUser = $this->createManagerUser();
        $user = factory(User::class)->create();
        $dorm = factory(Dorm::class)->create();
        $room = factory(Room::class)->create([
            'dorm_id' => $dorm->id
        ]);

        DB::table('occupied_rooms')->insert([
            [
                'user_id' => $user->id,
                'room_id' => $room->id,
                'paid_until' => Carbon::now()
            ]
        ]);

        $response = $this->post('/management/oc-room/payment-warning-mail', [
            'user_id' => $user->id,
            'room_id' => $room->id,
        ]);

        $response->assertStatus(200);

        //when paid_until is null, do this:

        $user = factory(User::class)->create();
        $dorm = factory(Dorm::class)->create();
        $room = factory(Room::class)->create([
            'dorm_id' => $dorm->id
        ]);

        DB::table('occupied_rooms')->insert([
            [
                'user_id' => $user->id,
                'room_id' => $room->id,
                'paid_until' => null,
            ]
        ]);

        $response = $this->post('/management/oc-room/payment-warning-mail', [
            'user_id' => $user->id,
            'room_id' => $room->id,
        ]);

        $response->assertStatus(200);
    }

    public function test_manager_can_accept_room_request()
    {
        $managerUser = $this->createManagerUser();
        $user = factory(User::class)->create();
        $dorm = factory(Dorm::class)->create();
        $room = factory(Room::class)->create([
            'dorm_id' => $dorm->id
        ]);

        $response = $this->post('/management/r-request/add', [
            'user' => $user->id,
            'room' => $room->id
        ]);

        $response->assertStatus(200);

        //check if user already has a room
        $response = $this->post('/management/r-request/add', [
            'user' => $user->id,
            'room' => $room->id
        ]);

        $response->assertStatus(200);

        //check if room has slots

        $user = factory(User::class)->create();
        $room = factory(Room::class)->create([
            'dorm_id' => $dorm->id,
            'slots' => 0
        ]);

        $response = $this->post('/management/r-request/add', [
            'user' => $user->id,
            'room' => $room->id
        ]);

        $response->assertStatus(200);
    }

    public function test_manager_can_delete_occupation()
    {
        $managerUser = $this->createManagerUser();
        $user = factory(User::class)->create();
        $dorm = factory(Dorm::class)->create();
        $room = factory(Room::class)->create([
            'dorm_id' => $dorm->id
        ]);

        DB::table('occupied_rooms')->insert([
            [
                'user_id' => $user->id,
                'room_id' => $room->id,
                'paid_until' => Carbon::now()
            ]
        ]);

        $response = $this->delete('/management/oc-room/delete/' . $user->id . '/' . $room->id);
        $response->assertStatus(200);
    }

    private function createManagerUser()
    {
        $managerUser = factory(User::class)->create();
        DB::table('role_user')->insert([
            [
                'user_id' => $managerUser->id,
                'role_id' => 2
            ]
        ]);

        $this->actingAs($managerUser, 'web');
        return $managerUser;
    }
}
