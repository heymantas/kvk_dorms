<?php

namespace Tests\Feature;

use App\Models\Dorm;
use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\DB;
use Tests\TestCase;
use Faker\Factory as Faker;

class DormTest extends TestCase
{
    use DatabaseTransactions;

    public function test_get_all_dorms_with_calculations()
    {
        $response = $this->get('/api/dorms');
        $response->assertStatus(200);
    }

    public function test_get_one_dorm_with_calculations()
    {
        $response = $this->get('/api/dorm/1/gender/1/true');
        $response->assertStatus(200);

        $response = $this->get('/api/dorm/1/gender/null/true');
        $response->assertStatus(200);
    }

    public function test_get_manager_dorms()
    {
        $managerUser = $this->createManagerUser();
        $response = $this->get('/management/dorm/user/' . $managerUser->id);
        $response->assertStatus(200);
    }

    public function test_get_one_dorm()
    {
        $managerUser = $this->createManagerUser();
        $dorm = factory(Dorm::class)->create();
        $response = $this->get('/management/dorm/get/' . $dorm->id);
        $response->assertStatus(200);
    }

    public function test_get_dorm_occupants_count()
    {
        $managerUser = $this->createManagerUser();
        $dorm = factory(Dorm::class)->create();
        $response = $this->get('/management/dorm/' . $dorm->id . '/occupants');
        $response->assertStatus(200);
    }

    public function test_get_dorm_room_requests()
    {
        $managerUser = $this->createManagerUser();
        $dorm = factory(Dorm::class)->create();
        $response = $this->get('/management/dorm/' . $dorm->id . '/r-requests/all');
        $response->assertStatus(200);
    }

    public function test_update_dorm()
    {
        $managerUser = $this->createManagerUser();
        $dorm = factory(Dorm::class)->create();

        DB::table('dorm_managers')->insert([
            'user_id' => $managerUser->id,
            'dorm_id' => $dorm->id
        ]);

        $photos[0]['path'] = 'images/dorms/default_dorm.png';

        $response = $this->put('/management/dorm/updatemng', [
            'id' => $dorm->id,
            'name' => $dorm->name,
            'photos' => $photos
        ]);

        $response->assertStatus(200);
    }

    public function test_manager_can_add_dorm()
    {
        $managerUser = $this->createManagerUser();
        $faker = Faker::create();
        $dormArray = [
            'name' => $faker->name,
            'address' => $faker->address,
            'lat' => '55.7184307',
            'long' => '21.1161138',
            'university_id' => '1',
            'manager_id' => $managerUser->id,
        ];

        $response = $this->post('/management/dorm/add-without-img', $dormArray);
        $response->assertStatus(200);
    }

    public function test_admin_can_assign_dorms_to_manager()
    {
        $this->createAdminUser();
        $managerUser = factory(User::class)->create();
        DB::table('role_user')->insert([
            [
                'user_id' => $managerUser->id,
                'role_id' => 2
            ]
        ]);

        $dorm = factory(Dorm::class)->create([
            'manager_id' => null
        ]);

        $response = $this->post('/admin/dorm/asignManager', [
            'id' => $dorm->id,
            'manager_id' => $managerUser->id
        ]);

        $response->assertStatus(200);
    }

    public function test_admin_can_unassign_dorm_from_manager()
    {
        $this->createAdminUser();
        $managerUser = factory(User::class)->create();
        DB::table('role_user')->insert([
            [
                'user_id' => $managerUser->id,
                'role_id' => 2
            ]
        ]);

        $dorm = factory(Dorm::class)->create([
            'manager_id' => $managerUser->id
        ]);
        $response = $this->delete('/admin/dorm/deleteAsignedManager/' . $dorm->id);
        $response->assertStatus(200);
    }

    public function test_admin_can_get_all_dorms()
    {
        $this->createAdminUser();
        $response = $this->get('/admin/dorm/all');
        $response->assertStatus(200);
    }

    public function test_admin_can_get_dorms_by_university_id()
    {
        $this->createAdminUser();
        $response = $this->get('/admin/dorm/all/1');
        $response->assertStatus(200);
    }

    public function test_admin_can_get_dorm_count()
    {
        $this->createAdminUser();
        $response = $this->get('/admin/dorm/count');
        $response->assertStatus(200);
    }

    public function test_admin_can_delete_dorm()
    {
        $this->createAdminUser();
        $dorm = factory(Dorm::class)->create();
        $response = $this->delete('/admin/dorm/delete/'. $dorm->id);
        $response->assertStatus(200);
    }

    private function createManagerUser()
    {
        $managerUser = factory(User::class)->create();
        DB::table('role_user')->insert([
            [
                'user_id' => $managerUser->id,
                'role_id' => 2
            ]
        ]);

        $this->actingAs($managerUser, 'web');
        return $managerUser;
    }

    private function createAdminUser()
    {
        $adminUser = factory(User::class)->create();
        DB::table('role_user')->insert([
            [
                'user_id' => $adminUser->id,
                'role_id' => 1
            ]
        ]);

        $this->actingAs($adminUser, 'web');
    }
}
