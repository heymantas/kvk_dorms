<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Laravel\Socialite\Facades\Socialite;
use Mockery;
use Mockery\Mock;
use Tests\TestCase;
use TheSeer\Tokenizer\Token;

class AuthTest extends TestCase
{
    use DatabaseTransactions;

    public function test_user_can_login()
    {
        $user = factory(User::class)->create();
        $response = $this->post('/login', [
            'email' => $user->email,
            'password' => "secret"
        ]);

        $response->assertRedirect('/');

        $response = $this->get('/refreshToken');
        $response->assertStatus(200);
    }

    public function test_user_can_send_password_reminder()
    {
        $user = factory(User::class)->create();
        $response = $this->post('/password/email', [
            'email' => $user->email
        ]);

        $response->assertRedirect('/');
    }

    public function test_user_can_register()
    {
        $response = $this->post('/register', [
            'email' => "john@example.com",
            'password' => "123456789",
            "password_confirmation" => "123456789",
            "name" => "John",
            'surname' => "Johnson",
            'phone' => "+37066666",
            'gender' => 1,
            'university_id' => 1
        ]);

        $response->assertStatus(200);
    }

    public function test_user_can_resend_verification()
    {
        //user email not verified
        $user = factory(User::class)->create([
            'email_verified_at' => null
        ]);
        $this->actingAs($user);
        $response = $this->get('/email/resend');
        $response->assertStatus(200);

        //user email verified, so just redirect
        $user = factory(User::class)->create();
        $this->actingAs($user);
        $response = $this->get('/email/resend');
        $response->assertRedirect('/');
    }

    public function test_can_get_authorized_user_info()
    {
        $user = factory(User::class)->create();
        $this->actingAs($user, 'api');
        $response = $this->get('/api/me');
        $response->assertStatus(200);
    }

    public function test_user_can_login_with_google()
    {
        //not done yet
        //$this->withoutExceptionHandling();
        $response = $this->post('/sociallogin/google');
        $response->assertOk();
    }

    public function test_google_callback()
    {
        $response = $this->get('/auth/google/callback');
        $response->assertStatus(200);
    }

}
