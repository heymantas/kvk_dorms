<?php

namespace Tests\Feature;

use App\Models\Dorm;
use App\Models\Room;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class RoomTest extends TestCase
{
    use DatabaseTransactions;

    public function test_room_search()
    {
        $dorm = factory(Dorm::class)->create();
        $response = $this->get('/api/dorm/' . $dorm->id . '/floor/1/gender/1/true');
        $response->assertStatus(200);

        $response = $this->get('/api/dorm/' . $dorm->id . ' /floor/1/gender/null/true');
        $response->assertStatus(200);
    }

    public function test_get_room_by_id_and_dorm_id()
    {
        $dorm = factory(Dorm::class)->create();
        $response = $this->get('/api/dorm/' . $dorm->id . '/room/1');
        $response->assertStatus(200);
    }

    public function test_get_all_rooms_for_manager()
    {
        $managerUser = $this->createManagerUser();
        $dorm = factory(Dorm::class)->create();
        $response = $this->get('/management/room/' . $dorm->id . '/all');
        $response->assertStatus(200);
    }

    public function test_manager_can_delete_room()
    {
        $managerUser = $this->createManagerUser();
        $dorm = factory(Dorm::class)->create();
        $room = factory(Room::class)->create([
            'dorm_id' => $dorm->id
        ]);

        $response = $this->delete('/management/room/' . $room->id . '/delete');
        $response->assertStatus(200);
    }

    public function test_admin_can_get_all_rooms()
    {
        $this->createAdminUser();
        $response = $this->get('/admin/room/all');
        $response->assertStatus(200);
    }

    public function test_admin_can_search_rooms_by_dorm()
    {
        $this->createAdminUser();
        $dorm = factory(Dorm::class)->create();
        $response = $this->get('/admin/room/search/' . $dorm->id);
        $response->assertStatus(200);
    }

    public function test_manager_can_add_room()
    {
        $this->createManagerUser();
        $dorm = factory(Dorm::class)->create();
        $features = array();

        $features[0]['text'] = "Nemokamas WiFi";
        $features[0]['value'] = 1;

        //test when validation is successfull
        $response =  $this->post('/management/room/add', [
            'room_number' => 999,
            'floor' => 9,
            'slots' => 3,
            'dorm_id' => $dorm->id,
            'price' => 55,
            'gender' => 1,
            'features' => $features,
            'photos' => [],
            'description' => "best room ever"
        ]);

        $response->assertStatus(200);

        //same test again to check if u can add the same thing again
        $response =  $this->post('/management/room/add', [
            'room_number' => 999,
            'floor' => 9,
            'slots' => 3,
            'dorm_id' => $dorm->id,
            'price' => 55,
            'gender' => 1,
            'features' => $features,
            'photos' => [],
            'description' => "best room ever"
        ]);

        $response->assertStatus(200);

        //test if validation can fail
        $response =  $this->post('/management/room/add', [
            'room_number' => 0
        ]);

        $response->assertStatus(200);
    }

    public function test_manager_can_update_room()
    {
        $this->withoutExceptionHandling();
        $this->createManagerUser();
        $dorm = factory(Dorm::class)->create();
        $room = factory(Room::class)->create([
            'dorm_id' => $dorm->id
        ]);

        $features = array();
        $features[0]['text'] = "Nemokamas WiFi";
        $features[0]['value'] = 1;

        //test when validation is successfull
        $response = $this->put('/management/room/update', [
            'id' => $room->id,
            'room_number' => 999,
            'floor' => 9,
            'slots' => 3,
            'dorm_id' => $dorm->id,
            'price' => 55,
            'gender' => 1,
            'features' => $features,
            'photos' => [],
            'description' => "best room ever"
        ]);

        $response->assertStatus(200);

        //same test again to check if u can add the same thing again
        $otherRoom = factory(Room::class)->create([
            'dorm_id' => $dorm->id,
            'room_number' => 123
        ]);

        $response =  $this->put('/management/room/update', [
            'id' => $room->id,
            'room_number' => $otherRoom->room_number,
            'floor' => 9,
            'slots' => 3,
            'dorm_id' => $dorm->id,
            'price' => 55,
            'gender' => 1,
            'features' => $features,
            'photos' => [],
            'description' => "best room ever"
        ]);

        $response->assertStatus(200);

        //test if validation can fail
        $response =  $this->put('/management/room/update', [
            'id' => $room->id,
            'room_number' => 0
        ]);

        $response->assertStatus(200);
    }

    public function test_manager_can_get_room_by_id()
    {
        $this->createManagerUser();
        $room = factory(Room::class)->create();
        $response = $this->get('/management/room/get/' . $room->id);
        $response->assertStatus(200);
    }

    public function test_admin_get_room_count()
    {
        $this->createAdminUser();
        $response = $this->get('/admin/room/count');
        $response->assertStatus(200);
    }

    private function createManagerUser()
    {
        $managerUser = factory(User::class)->create();
        DB::table('role_user')->insert([
            [
                'user_id' => $managerUser->id,
                'role_id' => 2
            ]
        ]);

        $this->actingAs($managerUser, 'web');
        return $managerUser;
    }

    private function createAdminUser()
    {
        $adminUser = factory(User::class)->create();
        DB::table('role_user')->insert([
            [
                'user_id' => $adminUser->id,
                'role_id' => 1
            ]
        ]);

        $this->actingAs($adminUser, 'web');
    }
}
