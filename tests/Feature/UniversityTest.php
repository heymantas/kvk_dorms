<?php

namespace Tests\Feature;

use App\Models\University;
use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\DB;
use Tests\TestCase;

class UniversityTest extends TestCase
{
    use DatabaseTransactions;

    /** @test */
    public function all_universities()
    {
        $response = $this->get('/api/university/list');
        $response->assertStatus(200);
    }

    /** @test */
    public function get_universities_by_city()
    {
        $response = $this->get('/api/university/getCity/1');
        $response->assertStatus(200);
    }

    /** @test */
    public function get_university_by_id()
    {
        $response = $this->get('/api/university/1');
        $response->assertStatus(200);
    }

    public function test_admin_can_see_university_list_page()
    {
        $this->createAdminUser();
        $response = $this->get('/admin/university');
        $response->assertStatus(200);
    }

    public function test_admin_can_see_create_uni_page()
    {
        $this->createAdminUser();
        $response = $this->get('/admin/university/create');
        $response->assertStatus(200);
    }

    public function test_admin_can_get_all_unis()
    {
        $this->createAdminUser();

        //returns universities with cities and dorms, orders by created_at, also paginates
        $response = $this->get('/admin/university/all');
        $response->assertStatus(200);

        //returns universities with cities and orders by name
        $response = $this->get('/admin/university/list');
        $response->assertStatus(200);
    }

    public function test_admin_can_create_uni()
    {
        $this->createAdminUser();
        $response = $this->post('/admin/university/add', [
            "name" => "A random uni",
            "city_id" => 1
        ]);

        $response->assertStatus(200);
    }

    public function test_admin_can_update_uni()
    {
        $this->createAdminUser();
        $response = $this->put('/admin/university/update', [
            'id' => 1,
            'name' => "Changed Uni name",
            'city_id' => 1
        ]);

        $response->assertStatus(200);
    }

    public function test_admin_can_delete_uni()
    {
        $this->createAdminUser();
        $response = $this->delete('/admin/university/delete/1');
        $response->assertStatus(200);
    }

    public function getUniversity($id) {
        return University::findOrFail($id);
    }

    public function test_get_uni_count()
    {
        $this->createAdminUser();
        $response = $this->get('/admin/university/count');
        $response->assertStatus(200);
    }

    public function test_admin_can_search_unies_by_name()
    {
        $this->createAdminUser();
        $response = $this->get('/admin/university/search');
        $response->assertStatus(200);
    }

    private function createAdminUser()
    {
        $adminUser = factory(User::class)->create();
        DB::table('role_user')->insert([
            [
                'user_id' => $adminUser->id,
                'role_id' => 1
            ]
        ]);

        $this->actingAs($adminUser, 'web');
    }
}
