<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CityTest extends TestCase
{
    /** @test */
    public function all_cities()
    {
        $response = $this->get('/api/cities');
        $response->assertStatus(200);
    }

    /** @test */
    public function get_city_by_id() {
        $response = $this->get('/api/city/get/1');
        $response->assertStatus(200);
    }
}
