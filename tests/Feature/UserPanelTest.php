<?php

namespace Tests\Feature;

use App\Models\Room;
use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Tests\TestCase;
use Faker\Factory as Faker;

class UserPanelTest extends TestCase
{

    use DatabaseTransactions;

    /** @test */
    public function get_user_occupied_room()
    {
        $user = factory(User::class)->create();
        $room = factory(Room::class)->create();

        DB::table('occupied_rooms')->insert([
            [
                'user_id' => $user->id,
                'room_id' => $room->id
            ]
        ]);

        $this->actingAs($user, 'api');
        $response = $this->get('/api/occupied-room/user/' . $user->id);
        $response->assertStatus(200);
    }

    /** @test */
    public function get_user_room_request()
    {
        $user = factory(User::class)->create();
        $room = factory(Room::class)->create();

        DB::table('room_requests')->insert([
            [
                'user_id' => $user->id,
                'room_id' => $room->id
            ]
        ]);

        $this->actingAs($user, 'api');
        $response = $this->get('/api/room-request/user/' . $user->id);
        $response->assertStatus(200);
    }

    /** @test */
    public function get_roommates()
    {
        $user = factory(User::class)->create();
        $room = factory(Room::class)->create();

        $this->actingAs($user, 'api');
        $response = $this->get('/api/roommates/room/' . $room->id);
        $response->assertStatus(200);
    }

    /** @test */
    public function can_delete_room_request()
    {
        $user = factory(User::class)->create();
        $room = factory(Room::class)->create();

        DB::table('room_requests')->insert([
            [
                'user_id' => $user->id,
                'room_id' => $room->id
            ]
        ]);

        $this->actingAs($user, 'api');
        $response = $this->delete('/api/room-request/delete/user/' . $user->id);
        $response->assertStatus(200);
    }

    /** @test */
    public function can_delete_yourself()
    {
        $user = factory(User::class)->create();
        $this->actingAs($user, 'api');
        $response = $this->delete('/api/user/delete/' . $user->id);
        $response->assertStatus(200);

    }

    /** @test */
    public function can_update_user_info()
    {
        $user = factory(User::class)->create();
        $this->actingAs($user, 'api');

        $response = $this->json('POST', '/api/user/edit/' . $user->id, [
            "name" => $user->name,
            "surname" => $user->surname,
            "phone" => $user->phone,
            "university_id" => $user->university_id,
            "photo" => $user->photo
        ]);

        $response->assertStatus(200);
    }

    /** @test */
    public function can_update_user_email()
    {
        $user = factory(User::class)->create();
        $this->actingAs($user, 'api');

        $response = $this->json('POST', '/api/user/email/edit/' . $user->id, [
            "email" => "test@test.lt"
        ]);

        $response->assertStatus(200);
    }

}
