<?php

namespace Tests\Feature;

use App\Models\Dorm;
use App\Models\Room;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Support\Facades\DB;
use Tests\TestCase;

class RoomRequestTest extends TestCase
{
    use DatabaseTransactions;

    public function test_check_if_user_has_request()
    {
        $user = factory(User::class)->create();
        $room = factory(Room::class)->create();

        $response = $this->get('/api/room-request/check/user/' . $user->id);
        $response->assertStatus(200);

        DB::table('room_requests')->insert([
            [
                'user_id' => $user->id,
                'room_id' => $room->id
            ]
        ]);

        $response = $this->get('/api/room-request/check/user/' . $user->id);
        $response->assertStatus(200);

    }

    public function test_check_if_user_has_occupation()
    {
        $user = factory(User::class)->create();
        $room = factory(Room::class)->create();

        $response = $this->get('/api/room-occupation/check/user/' . $user->id);
        $response->assertStatus(200);

        DB::table('occupied_rooms')->insert([
            [
                'user_id' => $user->id,
                'room_id' => $room->id
            ]
        ]);

        $response = $this->get('/api/room-occupation/check/user/' . $user->id);
        $response->assertStatus(200);
    }

    public function test_can_post_request()
    {
        $user = factory(User::class)->create();
        $room = factory(Room::class)->create();

        $this->actingAs($user, 'api');

        $response = $this->json('POST', '/api/room-request/create', [
            'user_id' => $user->id,
            "room_id" => $room->id
        ]);
        $response->assertStatus(200);

        //if validator fails (for example, no room_id found)
        $response = $this->json('POST', '/api/room-request/create', [
            'user_id' => $user->id,
        ]);
        $response->assertStatus(200);
    }

    public function test_manager_can_delete_room_request()
    {
        $managerUser = $this->createManagerUser();
        $user = factory(User::class)->create();
        $dorm = factory(Dorm::class)->create();
        $room = factory(Room::class)->create([
            'dorm_id' => $dorm->id
        ]);

        DB::table('room_requests')->insert([
            [
                'user_id' => $user->id,
                'room_id' => $room->id,
            ]
        ]);

        $response = $this->delete('/management/room-requests/delete/' . $user->id . '/' . $room->id);
        $response->assertStatus(200);
    }

    public function test_admin_can_get_request_by_user_id() {
        $this->createAdminUser();
        $user = factory(User::class)->create();
        $response = $this->get('/admin/room-requests/user/' . $user->id);
        $response->assertStatus(200);
    }

    public function test_admin_can_get_all_requests()
    {
        $this->createAdminUser();
        $user = factory(User::class)->create();
        $response = $this->get('/admin/room-requests/all');
        $response->assertStatus(200);
    }

    private function createManagerUser()
    {
        $managerUser = factory(User::class)->create();
        DB::table('role_user')->insert([
            [
                'user_id' => $managerUser->id,
                'role_id' => 2
            ]
        ]);

        $this->actingAs($managerUser, 'web');
        return $managerUser;
    }

    private function createAdminUser()
    {
        $adminUser = factory(User::class)->create();
        DB::table('role_user')->insert([
            [
                'user_id' => $adminUser->id,
                'role_id' => 1
            ]
        ]);

        $this->actingAs($adminUser, 'web');
    }
}
