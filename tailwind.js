module.exports = {
  theme: {
    screens: {
      'xs': '320px',
      'xl': '1280px'
    },
  },

  variants: {
    opacity: ['responsive', 'hover', 'focus', 'disabled'],
  },
}
