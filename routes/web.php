<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


use App\Models\DormManager;
use Illuminate\Support\Facades\Auth;

Route::get('/', function () {
    return view('index');
})->name('index');

Route::post('sociallogin/{provider}', 'AuthController@SocialSignup');
Route::get('auth/google/callback', 'AuthController@SocialCallBack');
Route::get('/refreshToken', 'AuthController@refreshToken')->name('refreshToken')->middleware('auth:web');
Route::get('/authCheck', function () {
    return response('', 200);
})->middleware('auth:web');
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/logout', 'Auth\LoginController@logout');
Auth::routes(['verify' => true]);

Route::group(['middleware' => ['auth:web', 'role:Manager'], 'prefix' => 'api/management', 'as' => 'api/management.'], function () {
    Route::post('/dorm/add-without-img', [
        'as' => 'dorm.add-without-img',
        'uses' => 'DormController@addDormWithoutImg'
    ]);
});

Route::group(['middleware' => ['auth:web', 'role:Administrator'], 'prefix' => 'api/admin', 'as' => 'api/admin.'], function () {
    Route::post('/dorm/add-without-img', [
        'as' => 'dorm.add-without-img',
        'uses' => 'DormController@addDormWithoutImg'
    ]);
});


// MANAGER ROUTER

Route::get('/panel', function () {
    if (Auth::check()) { //Patikrinama, ar vartotojas prisijungęs
        if (Auth::user()->hasRole('Administrator')) {
            return view('panel/admin/pages/home');
            //Jeigu rolė yra "administrator", turi rodyti bendrabučio valdytojo skydelį.
            //Šis skydelis turi visą funkcionalumą, reikalinga bendrabučio valdytojui

        } else if (Auth::user()->hasRole('Manager')) {
            $hasDorm = DormManager::where('user_id', Auth::user()->id)->first();
            if ($hasDorm) {
                return view('panel/management/pages/home');
                //Jeigu rolė yra "manager" ir yra priskirtas bendrabutis, turi rodyti
                //bendrabučio administratoriaus skydelį
            } else {
                return redirect('/management/register')->
                with('message', 'Atsiprašome, tačiau jūs neturite bendrabučio! Užregistruokite savo bendrabutį.');
                //Jeigu bendrabučio administratoriui nėra priskirtas bendrabutis, nukreipti į bendrabučio
                //registracijos puslapį
            }
        }
    }
    return redirect()->route('index'); //neprisijungęs vartotojas nukreipiamas į pagr. pusl.
})->name('panel');

Route::get('/manager/register', [
    'as' => 'manager.register',
    'uses' => 'ManagerController@registerManager',
])->middleware('signed');

Route::post('/manager/register', [
    'as' => 'manager.register',
    'uses' => 'ManagerController@store',
])->middleware('signed');

//ADMIN ROUTES HERE
Route::group(['middleware' => ['auth:web', 'role:Administrator'], 'prefix' => 'admin', 'as' => 'admin.'], function () {

    Route::get('/', [
        'as' => 'home',
        'uses' => 'Admin\AdminController@index',
    ]);

    // Users

    Route::get('/users', [
        'as' => 'users',
        'uses' => 'Admin\AdminController@showUsers',
    ]);

    Route::get('/user/view/{id}', [
        'as' => 'user',
        'uses' => 'Admin\AdminController@showUser',
    ]);

    Route::get('/user/edit/{id}', [
        'as' => 'user.edit',
        'uses' => 'Admin\AdminController@editUser',
    ]);

    Route::get('/user/create', [
        'as' => 'user.create',
        'uses' => 'Admin\AdminController@createUser',
    ]);

    //Managers

    Route::get('/managers', [
        'as' => 'managers',
        'uses' => 'Admin\AdminController@managers',
    ]);

    Route::get('/manager/invite', [
        'as' => 'manager.invite',
        'uses' => 'Admin\AdminController@inviteManager',
    ]);

    Route::post('/manager/email/send', [
        'as' => 'manager.email.send',
        'uses' => 'ManagerController@emailManager',
    ]);

    //Permissions

    Route::get('/permissions', [
        'as' => 'permissions',
        'uses' => 'Admin\AdminController@showPermissions',
    ]);

    Route::get('/permissions/create', [
        'as' => 'permissions.create',
        'uses' => 'Admin\AdminController@createPermissions',
    ]);

    //University

    Route::get('/university', [
        'as' => 'university',
        'uses' => 'UniversityController@index',
    ]);
    Route::get('/university/create', [
        'as' => 'university.create',
        'uses' => 'UniversityController@createUniversity',
    ]);

    //Dorms

    Route::get('/dorms', [
        'as' => 'dorms',
        'uses' => 'DormController@showDorms',
    ]);
    Route::get('/dorm/create/{id}', [
        'as' => 'dorm.create',
        'uses' => 'DormController@createDorm',
    ]);

    //Rooms

    Route::get('/rooms', [
        'as' => 'rooms',
        'uses' => 'Admin\AdminController@showRooms',
    ]);

    Route::get('/room/edit/{id}', [
        'as' => 'room.edit',
        'uses' => 'Admin\AdminController@editRoom',
    ]);

    Route::get('/room/create', [
        'as' => 'room.create',
        'uses' => 'Admin\AdminController@createRoom',
    ]);

    //Rooms requests

    Route::get('/r-requests', [
        'as' => 'roomrequests',
        'uses' => 'Admin\AdminController@showRoomRequests',
    ]);

    //*** Admin API routes ***//

    // User
    Route::get('/user/get/{id}', [
        'as' => 'user.get',
        'uses' => 'UserController@getUser',
    ]);

    Route::get('/user/all', [
        'as' => 'user.all',
        'uses' => 'UserController@all',
    ]);

    Route::get('/user/managers', [
        'as' => 'user.managers',
        'uses' => 'UserController@getManagers',
    ]);


    Route::get('/user/search', [
        'as' => 'user.search',
        'uses' => 'UserController@search',
    ]);

    Route::put('/user/update/{id}', [
        'as' => 'user.update',
        'uses' => 'UserController@update',
    ]);

    Route::delete('/user/delete/{id}', [
        'as' => 'user.delete',
        'uses' => 'UserController@delete',
    ]);

    Route::post('/user/add', [
        'as' => 'user.add',
        'uses' => 'UserController@add',
    ]);

    // Permissions
    Route::get('/permissions/all', [
        'as' => 'permissions.all',
        'uses' => 'PermissionController@all',
    ]);

    Route::get('/permissions/search', [
        'as' => 'permissions.search',
        'uses' => 'PermissionController@search',
    ]);

    Route::delete('/permissions/delete/{permission_id}', [
        'as' => 'permissions.delete',
        'uses' => 'PermissionController@delete',
    ]);

    Route::post('/permissions/add', [
        'as' => 'permissions.add',
        'uses' => 'PermissionController@add',
    ]);

    //PermissionRole
    Route::post('/permissionRole/add', [
//        'as' => 'permissionRole.add',
        'uses' => 'PermissionRoleController@add',
    ]);
    Route::delete('/permissionRole/delete/{role_id}&{permission_id}', [
        'as' => 'permissionRole.delete',
        'uses' => 'PermissionRoleController@delete',
    ]);

    //Room Requests

    Route::get('/room-requests/all', [
        'as' => 'roomrequest.all',
        'uses' => 'RoomRequestController@all',
    ]);

    Route::get('/room-requests/user/{id}', [
        'as' => 'roomrequest.user.get',
        'uses' => 'RoomRequestController@getByUser',
    ]);

    Route::delete('/room-requests/delete/{user_id}/{room_id}', [
        'as' => 'roomrequest.delete',
        'uses' => 'RoomRequestController@delete',
    ]);

    //*** Admin API routes end ***//


    //Counts

    Route::get('/user/count', [
        'as' => 'user.count',
        'uses' => 'UserController@count',
    ]);

    Route::get('/dorm/count', [
        'as' => 'user.count',
        'uses' => 'DormController@count',
    ]);

    Route::get('/university/count', [
        'as' => 'user.count',
        'uses' => 'UniversityController@count',
    ]);

    Route::get('/room/count', [
        'as' => 'user.count',
        'uses' => 'RoomController@count',
    ]);

    //Managers

    Route::get('/managers/search', [
        'as' => 'managers.search',
        'uses' => 'UserController@searchManagers',
    ]);

    //Occupied Rooms

    Route::post('/oc-room/add', [
        'as' => 'ocroom.add',
        'uses' => 'OccupiedRoomController@add',
    ]);

    //User Roles

    Route::get('/role/all', [
        'as' => 'role.all',
        'uses' => 'RoleController@all',
    ]);

    //Rooms

    Route::get('/room/all', [
        'as' => 'room.all',
        'uses' => 'RoomController@all',
    ]);

    Route::get('/room/search/{id}', [
        'as' => 'room.search',
        'uses' => 'RoomController@searchByDorm',
    ]);

    Route::post('/room/add', [
        'as' => 'room.add',
        'uses' => 'RoomController@add',
    ]);

    Route::delete('/room/delete/{roomId}', [
        'as' => 'room.delete',
        'uses' => 'RoomController@delete',
    ]);

    //Dorms

    Route::get('/dorm/all', [
        'as' => 'dorm.all',
        'uses' => 'DormController@all',
    ]);

    Route::post('/dorm/asignManager', [
        'as' => 'dorm.asignManager',
        'uses' => 'DormController@asignManager'
    ]);

    Route::delete('/dorm/deleteAsignedManager/{dorm_id}', [
        'as' => 'dorm.deleteAsignedManager',
        'uses' => 'DormController@deleteAsignedManager'
    ]);

    Route::delete('/dorm/delete/{dormId}', [
        'as' => 'dorm.all',
        'uses' => 'DormController@delete'
    ]);

    Route::get('/dorm/all/{university}', [
        'as' => 'dorm.all.university',
        'uses' => 'DormController@allByUni',
    ]);

    //Universities

    Route::get('/university/all', [
        'as' => 'university.all',
        'uses' => 'UniversityController@all',
    ]);

    Route::get('/university/list', [
        'as' => 'university.list',
        'uses' => 'UniversityController@list',
    ]);

    Route::get('/university/search', [
        'as' => 'university.search',
        'uses' => 'UniversityController@search',
    ]);

    Route::post('/university/add', [
        'as' => 'university.add',
        'uses' => 'UniversityController@add',
    ]);

    Route::put('/university/update', [
        'as' => 'university.update',
        'uses' => 'UniversityController@update',
    ]);

    Route::delete('/university/delete/{id}', [
        'as' => 'university.delete',
        'uses' => 'UniversityController@delete',
    ]);

});

// MANAGEMENT ROUTES HERE

Route::group(['middleware' => ['auth:web', 'role:Manager'], 'prefix' => 'management', 'as' => 'management.'], function () {

    Route::get('/', [
        'as' => 'home',
        'uses' => 'ManagementController@index',
    ]);

    Route::get('/register', [
        'as' => 'dorm.register',
        'uses' => 'ManagementController@registerDorm',
    ]);

    //*** Management API routes ***//

    //Get all users who haven't occupied any room yet
    Route::get('/user/free/all/', [
        'as' => 'management.user.free.all',
        'uses' => 'UserController@allMng',
    ]);

    // Get manager's dorms
    Route::get('/dorm/user/{managerId}', [
        'as' => 'dorm.user.get',
        'uses' => 'DormController@getUserDorm',
    ]);

    //Get dorm
    Route::get('/dorm/get/{id}', [
        'as' => 'dorm.get',
        'uses' => 'DormController@getDorm',
    ]);

    //Get dorm occupants' count
    Route::get('/dorm/{dorm}/occupants', [
        'as' => 'dorm.occupants',
        'uses' => 'DormController@getDormOccupants',
    ]);

    //Get dorm room requests
    Route::get('/dorm/{dorm}/r-requests/all', [
        'as' => 'dorm.room.requests.all',
        'uses' => 'DormController@getDormRoomRequests',
    ]);

    //Update dorm
    Route::put('/dorm/updatemng', [
        'as' => 'dorm.update.mng',
        'uses' => 'DormController@updateMng',
    ]);

    Route::get('/room/{dorm}/all', [
        'as' => 'room.dorm.all',
        'uses' => 'RoomController@getDormRooms',
    ]);

    Route::post('/room/add', [
        'as' => 'room.add',
        'uses' => 'RoomController@add',
    ]);

    Route::put('/room/update', [
        'as' => 'room.update',
        'uses' => 'RoomController@update',
    ]);

    Route::get('/room/get/{id}', [
        'as' => 'room.update',
        'uses' => 'RoomController@get',
    ]);

    Route::get('/feature/all', [
        'as' => 'feature.all',
        'uses' => 'FeatureController@all',
    ]);

    Route::delete('/room/{id}/delete', [
        'as' => 'room.delete',
        'uses' => 'RoomController@delete',
    ]);

    Route::put('/oc-room/update', [
        'as' => 'oc.room.update',
        'uses' => 'OccupiedRoomController@changeRoom',
    ]);

    Route::put('/oc-room/update-payment-date', [
        'as' => 'oc.room.update-payment-date',
        'uses' => 'OccupiedRoomController@updatePaymentDate',
    ]);

    Route::post('/oc-room/payment-warning-mail', [
        'as' => 'oc.room.payment-warning-mail',
        'uses' => 'OccupiedRoomController@sendPaymentWarning',
    ]);

    //TEST
    Route::get('/api', [
        'as' => 'api',
        'uses' => 'ManagementController@api',
    ]);

    Route::get('/api2', [
        'as' => 'api2',
        'uses' => 'ManagementController@api2',
    ]);
    //TEST**

    Route::post('/r-request/add', [
        'as' => 'room.request.add',
        'uses' => 'OccupiedRoomController@add',
    ]);

    Route::delete('/room-requests/delete/{user_id}/{room_id}', [
        'as' => 'room.request.delete',
        'uses' => 'RoomRequestController@delete',
    ]);

    Route::delete('/oc-room/delete/{user_id}/{room_id}', [
        'as' => 'oc.room.delete',
        'uses' => 'OccupiedRoomController@delete',
    ]);

    //*** End Management API routes ***//

});


