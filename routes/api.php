<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('university/list','UniversityController@list');
Route::get('university/getCity/{id}','UniversityController@getCity');
Route::get('university/{id}', 'UniversityController@getUniversity');
Route::get('dorms', 'DormController@index');
Route::get('cities', 'CityController@all');
Route::get('city/get/{id}', 'CityController@getCity');
Route::get('dorm/{id}/gender/{gender}/{orderBySlots}', 'DormController@showOneDorm');
Route::get('dorm/{id}/floor/{floor}/gender/{gender}/{orderBySlots}', 'RoomController@searchByFloor');
Route::get('dorm/{dorm_id}/room/{room_id}', 'RoomController@getRoom');
Route::post('/room-request/create', 'RoomRequestController@add')->middleware('auth:api','verified');
Route::get('/room-request/check/user/{id}', 'RoomRequestController@checkRequest');
Route::get('/room-occupation/check/user/{id}', 'RoomRequestController@checkOccupation');
Route::get('/me','AuthController@me')->middleware('auth:api');

//API for User Panel
Route::get('occupied-room/user/{user_id}', 'UserPanelController@getUserOccupiedRoom')->middleware('auth:api');
Route::get('room-request/user/{user_id}', 'UserPanelController@getRequest')->middleware('auth:api');
Route::get('roommates/room/{room_id}', 'UserPanelController@getRoommates')->middleware('auth:api');
Route::delete('room-request/delete/user/{user_id}', 'UserPanelController@deleteRequest')->middleware('auth:api');
Route::post('user/edit/{id}', 'UserPanelController@updateUserInfo')->middleware('auth:api');
Route::post('user/email/edit/{id}', 'UserPanelController@updateEmail')->middleware('auth:api');
Route::delete('user/delete/{id}', 'UserPanelController@deleteUser')->middleware('auth:api');
