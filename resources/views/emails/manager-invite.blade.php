@component('mail::message')
# Sveiki!

Kviečiame prisijungti į sistemos 'KVK bendrabučiai' administratorius!

Paspaudus mygtuką, jums reikės užsiregistruoti. Poto galėsite susikurti bendrabutį!

@component('mail::button', ['url' => $link])
Registruotis
@endcomponent
Ši nuoroda nebegalios po 60 minučių.

Jeigu jokio prašymo nepateikėtę, nieko daryti nereikia.

Pagarbiai,<br>
KVK bendrabučiai
<br>
<p style="font-size:12px; color:f2f2f2">Jei nepavyksta paspausti mygtuką, nukopijuokite ir įklijuokite apačioje esančia nuorodą į naršyklę.<br>
<a href="{{$link}}">{{$link}}</a>
<br><br>
Šis laiškas yra automatinis, todėl atsakyti į jį nereikia.</p>
@endcomponent
