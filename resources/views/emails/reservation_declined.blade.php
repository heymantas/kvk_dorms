@component('mail::message')
# Sveiki, {{$user->name}}

Rezarvacija į bendrabučio {{$room->room_number}} kambarį atmesta. Bandykite pasirinkti kitą kambarį.

<br>
KVK bendrabučiai
@endcomponent
