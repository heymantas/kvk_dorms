@component('mail::message')
# Sveiki, {{$user->name}}!

Norime jumis įspėti, kad dar nesate susimokėję už savo kambarį. <br/>

Jūsų kambario informacija: <br/> <br/>
<b>{{$room->dorm->name}}</b> <br/>
{{$room->dorm->address}} ({{$university->city->name}})

<b>Kambario numeris</b> - {{$room->room_number}} <br/>
<b>Aukštas</b> - {{$room->floor}} <br/>
<b>Kaina/mėn</b> - {{$room->price}} € <br/>


Praeitą kartą susimokėta iki - <span style="color: red"><b>{{$date->paid_until}}</b></span> <br/>


Pagarbiai,<br>
KVK bendrabučiai
@endcomponent
