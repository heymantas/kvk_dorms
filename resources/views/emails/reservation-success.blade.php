@component('mail::message')
# Sveiki, {{$user->name}}!

Dėkojame už pateikta rezervaciją. <br>

![][success_image]
[success_image]: {{asset('/images/reservation-success.png')}}

<b>{{$room[0]->dorm->name}}</b> <br/>
{{$room[0]->dorm->address}} ({{$university[0]->city->name}})

<b>Kambario numeris</b> - {{$room[0]->room_number}} <br/>
<b>Aukštas</b> - {{$room[0]->floor}} <br/>
<b>Kaina/mėn</b> - {{$room[0]->price}} €

Apie rezervacijos patvirtinimą ar atmetimą bus pranešta el.paštu!

Pagarbiai,<br>
KVK bendrabučiai
@endcomponent
