<div class="hidden md:block sidenav w-1/6 bg-gray-400">
    <div class="sidenav-top h-16 p-4 shadow-md">
        <a class="pt-4" href="{{route('panel')}}">Bendrabučio valdytojas</a>
    </div>
    <div class="sidenav-mid m-4">
        <h1 class="font-semibold">Vartotojų administravimas</h1>
        <ul class="list-none mt-2">
            <a href="{{route('admin.users')}}" class="w-5/6"><li class="py-2 pl-2 flex justify-between">Vartotojai <a href="{{route('admin.user.create')}}" class="w-1/6"><i class="fas fa-plus"></i></a></li></a>
            <a href="{{route('admin.permissions')}}" class="w-5/6"><li class="py-2 pl-2 flex justify-between">Teisės <a href="{{route('admin.permissions.create')}}" class="w-1/6"><i class="fas fa-plus"></i></a></li></a>
        </ul>
        <h1 class="font-semibold">Mokyklų administravimas</h1>
        <ul class="list-none mt-2">
            <a href="{{route('admin.university')}}" class="w-5/6"><li class="py-2 pl-2 flex justify-between">Mokymosi įstaigos <a href="{{route('admin.university.create')}}" class="w-1/6"><i class="fas fa-plus"></i></a></li></a>
            <a href="{{route('admin.managers')}}" class="w-5/6"><li class="py-2 pl-2 flex justify-between">Administratoriai <a href="{{route('admin.manager.invite')}}" class="w-1/6"><i class="fas fa-plus"></i></a></li></a>
        </ul>
    </div>
</div>
