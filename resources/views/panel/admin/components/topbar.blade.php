<div class="main-nav text-white h-16 shadow-md">
    <a class="md:hidden mt-5 ml-5 float-left" href="{{route('panel')}}">Bendrabučio valdytojas</a>
    <a class="mt-5 mr-5 ml-2 float-right hidden sm:hidden md:inline lg:inline" href="{{route('logout')}}">Logout</a>
    <p class="mt-5 mr-5 float-right md:hidden" onclick="expandNav()"><i class="fas fa-bars"></i></p>
    <div id="nav" class="p-4 main-nav hidden md:hidden absolute z-20 w-full" style="top: 4rem">
        <h1 class="font-semibold">Vartotojų administravimas</h1>
        <ul class="list-none mt-2">
            <a href="{{route('admin.users')}}" class="w-5/6"><li class="py-2 pl-2 flex justify-between">Vartotojai <a href="{{route('admin.user.create')}}" class="w-1/6"><i class="fas fa-plus"></i></a></li></a>
            <a href="{{route('admin.permissions')}}" class="w-5/6"><li class="py-2 pl-2 flex justify-between">Teisės <a href="{{route('admin.permissions.create')}}" class="w-1/6"><i class="fas fa-plus"></i></a></li></a>
        </ul>
        <h1 class="font-semibold">Mokyklų administravimas</h1>
        <ul class="list-none mt-2">
            <a href="{{route('admin.university')}}" class="w-5/6"><li class="py-2 pl-2 flex justify-between">Mokymosi įstaigos <a href="{{route('admin.university.create')}}" class="w-1/6"><i class="fas fa-plus"></i></a></li></a>
            <a href="{{route('admin.managers')}}" class="w-5/6"><li class="py-2 pl-2 flex justify-between">Administratoriai <a href="{{route('admin.manager.invite')}}" class="w-1/6"><i class="fas fa-plus"></i></a></li></a>
        </ul>
        <ul class="list-none mt-5">
            <a class="mt-5 mr-5 ml-2" href="{{route('logout')}}">Logout</a>
        </ul>
    </div>
</div>
