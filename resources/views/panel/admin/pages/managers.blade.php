@extends('panel/admin/index')
@section('content-body')
<div class="content-body p-4">
    <admin-manager-list current-page="" route-all="{{route('admin.user.managers')}}" route-search="{{route('admin.managers.search')}}"></admin-manager-list>
</div>
@endsection
