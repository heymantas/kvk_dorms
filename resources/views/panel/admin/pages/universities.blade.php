@extends('panel/admin/index')
@section('content-body')
    <div class="content-body p-4">
        <admin-university-list current-page="" route-all="{{route('admin.university.all')}}" route-search="{{route('admin.university.search')}}"></admin-university-list>
    </div>
@endsection