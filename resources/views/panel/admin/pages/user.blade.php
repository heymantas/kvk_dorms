@extends('panel/admin/index')
@section('content-body')
    <div class="content-body flex">
        <div class="w-1/3 user-card mr-6 p-4">
            <div class="border-b-2 border-black pb-6 text-center">
                <h1 class="font-semibold text-xl">{{$user->name . ' ' . $user->surname}}</h1>
                <h1>Studentas (-ė)</h1>
            </div>
            <ul class="list-none p-0 pt-6">
                <li><span class="font-semibold">Mokymosi įstaiga:</span> {{$user->uni_name}}</li>
                <li><span class="font-semibold w-1/3">El. Paštas:</span> <span class="text-right">{{$user->email}}</span></li>
                <li><span class="font-semibold">Tel. Nr.:</span> <a href="tel:{{$user->phone}}">{{$user->phone}}</a></li>
            </ul>
        </div>
        <div class="w-2/3 p-4">
            <h1 class="font-semibold text-xl pb-6">Kambarių rezervacijos</h1>
            @foreach($roomRequests as $roomRequest)
                <div class="room-container mb-2 border-gray-300 border-2">
                    <div class="p-2 flex">
                        <img src="{{$roomRequest->photos}}" class="w-56 h-48">
                        <div class="room-info pl-6">
                            <h1><span class="font-semibold">Kambarys:</span> {{$roomRequest->room_number}}</h1>
                            <h1><span class="font-semibold">Bendrabutis:</span> {{$roomRequest->name}}, {{$roomRequest->address}}</h1>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
@endsection
