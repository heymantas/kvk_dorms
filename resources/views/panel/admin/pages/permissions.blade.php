@extends('panel/admin/index')
@section('content-body')
    <div class="content-body p-4">
        <admin-permissions-list current-page="" route-all="{{route('admin.permissions.all')}}" route-search="{{route('admin.permissions.search')}}" route-roles="{{route('admin.role.all')}}"></admin-permissions-list>
    </div>
@endsection