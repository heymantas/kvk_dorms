@extends('panel/admin/index')
@section('content-body')
    <div class="container">
        <div class="content-body p-8">
            <div class="w-full max-w-3xl m-auto">
                <div class="rounded overflow-hidden">
                    <div class="px-6 py-4">
                        <div class="font-bold text-xl mb-2">Pridėti administratorių</div>
                        <p class="text-gray-700 text-base mb-10">
                            Įvedus į laukelį el-paštą, asmuo gaus pakvietimą prisijungti prie sistemos administratorių.
                        </p>
                        <form action="{{ route('admin.manager.email.send') }}" method="post">
                            @csrf
                            <div class="mb-4">
                                <label for="email"
                                       class="block text-gray-700 text-sm font-bold mb-2">{{ __('El-paštas') }}</label>
                                <input id="email" type="email"
                                       class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                                       name="email" required autofocus>
                                @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong class="text-red-500 text-xs italic">{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                            <button class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline"
                                    type="submit">
                                Patvirtinti
                            </button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

