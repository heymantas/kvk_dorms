@extends('panel/admin/index')
@section('content-body')
<div class="content-body p-4">
    <admin-user-list current-page="" route-all="{{route('admin.user.all')}}" route-search="{{route('admin.user.search')}}"></admin-user-list>
</div>
@endsection
