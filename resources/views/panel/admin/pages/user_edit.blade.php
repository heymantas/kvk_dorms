@extends('panel/admin/index')
@section('content-body')
<div class="content-body p-4">
    <div>
        <edit-user-form route-room-request-get="{{route('admin.roomrequest.user.get', $user->id)}}" university-name="{{$user->university->name ?? ""}}" route-user-update="{{route('admin.user.update', $user->id)}}" route-user-get="{{route('admin.user.get', $user->id)}}" route-university-all="{{route('admin.university.list')}}"></edit-user-form>
    </div>
</div>
@endsection
