@extends('layouts.master')
@section('title')
KVK bendrabutis - valdymas
@endsection

@section('content')
    <div class="flex flex-wrap min-h-screen">
        @include('panel/admin/components/sidebar')
        <div class="w-full md:w-5/6">
            @include('panel/admin/components/topbar')
            @yield('content-body')
        </div>
    </div>
@endsection
