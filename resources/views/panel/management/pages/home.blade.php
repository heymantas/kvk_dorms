@extends('panel/management/index')
@section('content-body')
    <div class="content-body p-4">
        <router-view name="management"></router-view>
    </div>
@endsection
