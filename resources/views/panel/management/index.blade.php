@extends('layouts.master')
@section('title')
    Bendrabučių administravimas
@endsection

@section('content')
    <div class="flex flex-wrap min-h-screen">
        <Sidebar></Sidebar>
        <div class="w-full lg:w-5/6">
            <Topbar></Topbar>
            @yield('content-body')
        </div>
    </div>
    </div>
@endsection
