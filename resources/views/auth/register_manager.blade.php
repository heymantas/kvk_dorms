@extends('layouts/master')

@section('content')
<nav class="flex items-center justify-between flex-wrap bg-teal-500 p-6">
    <div class="flex container">
        <div class="flex items-center flex-shrink-0 text-white mr-6">
            <a href="/" class="font-semibold text-xl tracking-tight">Bendrabutis</a>
        </div>
    </div>
</nav>
<div class="w-full max-w-xs m-auto" style="margin-top:5%">
    <form class="bg-white shadow-md rounded px-8 pt-6 pb-8 mb-4" method="POST" action="{{url()->full()}}">
      <h1 class="text-center mb-5">Kurti paskyrą</h1>
      @csrf
      <div class="mb-4">
        <label for="email" class="block text-gray-700 text-sm font-bold mb-2">{{ __('El-paštas') }}</label>
        <input id="email" type="email" class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline @error('email') border-red-500 @enderror" name="email" value="{{ $email ?? old('email') }}" required autocomplete="email" autofocus disabled>

        @error('email')
            <span class="invalid-feedback" role="alert">
                <strong class="text-red-500 text-xs italic">{{ $message }}</strong>
            </span>
        @enderror
      </div>

      <div class="mb-6">
        <label class="block text-gray-700 text-sm font-bold mb-2" for="password">
            {{ __('Slaptažodis') }}
        </label>
        <input class="shadow appearance-none rounded w-full py-2 px-3 text-gray-700 mb-3 leading-tight focus:outline-none border focus:shadow-outline @error('password') border-red-500 @enderror" id="password" type="password" required autocomplete="new-password" name="password">
        @error('password')
            <span class="invalid-feedback" role="alert">
                <strong class="text-red-500 text-xs italic">{{ $message }}</strong>
            </span>
        @enderror
      </div>

        <div class="mb-6">
            <label class="block text-gray-700 text-sm font-bold mb-2" for="password-confirm">
                {{ __('Patvirtinti slaptažodį') }}
            </label>
            <input class="shadow appearance-none rounded w-full py-2 px-3 text-gray-700 mb-3 leading-tight focus:outline-none border focus:shadow-outline @error('password') border-red-500 @enderror" id="password-confirm" type="password" required autocomplete="new-password" name="password_confirmation">
        </div>

        <div class="mb-6">
            <label class="block text-gray-700 text-sm font-bold mb-2" for="name">
                {{ __('Vardas') }}
            </label>
            <input class="shadow appearance-none rounded w-full py-2 px-3 text-gray-700 mb-3 leading-tight focus:outline-none border focus:shadow-outline @error('name') border-red-500 @enderror" id="name" type="text" required autocomplete="given-name" name="name">
            @error('name')
                <span class="invalid-feedback" role="alert">
                    <strong class="text-red-500 text-xs italic">{{ $message }}</strong>
                </span>
            @enderror
        </div>

        <div class="mb-6">
            <label class="block text-gray-700 text-sm font-bold mb-2" for="surname">
                {{ __('Pavardė') }}
            </label>
            <input class="shadow appearance-none rounded w-full py-2 px-3 text-gray-700 mb-3 leading-tight focus:outline-none border focus:shadow-outline @error('surname') border-red-500 @enderror" id="surname" type="text" required autocomplete="family-name" name="surname">
            @error('surname')
                <span class="invalid-feedback" role="alert">
                    <strong class="text-red-500 text-xs italic">{{ $message }}</strong>
                </span>
            @enderror
        </div>

      <div class="flex items-center justify-center">
        <button class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline" type="submit">
          Patvirtinti
        </button>
      </div>
    </form>
  </div>
@endsection
