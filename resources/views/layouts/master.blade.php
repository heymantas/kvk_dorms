<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" id="token" content="{{ csrf_token() }}">
    <meta http-equiv="Content-Security-Policy" content="upgrade-insecure-requests">

    <title>@yield('title', 'KVK bendrabutis')</title>

    <!-- Styles -->
    <link rel="stylesheet" href="{{ mix('/css/app.css') }}"/>

    <!-- JavaScript -->
    <script defer src="{{ mix('/js/app.js')}}"></script>
</head>
<body>
    <div id="app">
        @yield('content')
    </div>
</body>

<script>
    function expandNav() {
        let element = document.getElementById("nav");
        element.classList.toggle("hidden");
    }
</script>

</html>
