@extends('layouts/master')
@section('content')
    <navbar-layout></navbar-layout>
    <br/>
    <router-view name="main" class="mt-20"></router-view>
@endsection
