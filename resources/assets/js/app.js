//Includes
import './bootstrap';
import Vue from 'vue';
import Vuex from 'vuex';
import VueRouter from 'vue-router';
import store from './store';
import VueSweetalert2 from 'vue-sweetalert2';
import router from './routes';
import VueAxios from 'vue-axios';
import VueSocialauth from 'vue-social-auth';
import VueExpandableImage from 'vue-expandable-image'
import axios from 'axios';
import 'sweetalert2/dist/sweetalert2.min.css';
import VueInitialsImg from 'vue-initials-img';

//Layout
//import Navbar from './layouts/navbar';
import VueLazyload from 'vue-lazyload';

window.Vue = require('vue');

Vue.use(VueLazyload);
Vue.use(VueRouter);
Vue.use(Vuex);
Vue.use(VueSweetalert2);
Vue.use(VueExpandableImage);
Vue.use(VueInitialsImg);

Vue.component('sidebar', require('./management/components/Sidebar').default);
Vue.component('topbar', require('./management/components/Topbar').default);

Vue.component('dorm-create', require('./management/pages/DormCreate').default);
Vue.component('navbar-layout', require('./layouts/Navbar.vue').default);
Vue.component('landing-map-component', require('./components/pages/main/Info.vue').default);
Vue.component('dorm-component', require('./components/pages/dorm-info/DormInfo.vue').default);
Vue.component('admin-home', require('./admin/pages/AdminHome.vue').default, {
	name: 'admin-home',
});

Vue.component('admin-dorm-create', require('./admin/pages/AdminDormCreate.vue').default, {
    name: 'admin-room-create',
});

//Room
Vue.component('admin-room-create', require('./admin/pages/AdminRoomCreate.vue').default, {
	name: 'admin-room-create',
});

Vue.component('admin-room-list', require('./admin/pages/AdminRoomlist.vue').default, {
	name: 'admin-room-list',
});

Vue.component('admin-room-requests', require('./admin/pages/AdminRoomRequests.vue').default, {
	name: 'admin-room-requests',
});

//User
Vue.component('admin-user-list', require('./admin/pages/AdminUserList.vue').default, {
	name: 'admin-user-list',
});

Vue.component('edit-user-form', require('./admin/pages/EditUserForm.vue').default, {
	name: 'edit-user-form',
});

Vue.component('admin-user-create', require('./admin/pages/AdminUserCreate.vue').default, {
	name: 'admin-user-create',
});

//Managers

Vue.component('admin-manager-list', require('./admin/pages/AdminManagerList.vue').default, {
	name: 'admin-manager-list',
});

//Permission
Vue.component('admin-permissions-list', require('./admin/pages/AdminPermissionsList.vue').default, {
	name: 'admin-permissions-list',
});

Vue.component('admin-permissions-create', require('./admin/pages/AdminPermissionsCreate.vue').default, {
	name: 'admin-permissions-create',
});

//University
Vue.component('admin-university-list', require('./admin/pages/AdminUniversityList.vue').default, {
	name: 'admin-university-list',
});

Vue.component('admin-university-create', require('./admin/pages/AdminUniversityCreate.vue').default, {
	name: 'admin-university-create',
});

Vue.use(VueAxios, axios);
Vue.use(VueSocialauth, {

	providers: {
		google: {
			clientId: '505475544375-ba7nc9tlf3cr7vfo2u68afeq0i1b01ju.apps.googleusercontent.com',
			redirectUri: 'https://eimantas.tech/auth/google/callback' // Your client app URL
		}
	}
});

//Router check auth
router.beforeEach((to, from, next) => { //Check user/role
	if (to.matched.some(record => record.meta.requiresAuth)) {
		if (store.state.auth == null) {
			Vue.swal({
				title: 'Prašome prisijungti',
				type: 'error',
				buttonsStyling: false,
				customClass: {
					confirmButton: 'bg-transparent m-2 hover:bg-teal-700 text-teal-900' +
                        'font-semibold hover:text-white py-2 px-4 border border-teal-700' +
                        'hover:border-transparent rounded',
				}
			});
			next({
				name: 'index',
			});
		} else {
			let user = JSON.parse(localStorage.getItem('user'));
			if (to.matched.some(record => record.meta.is_admin)) {
				if (user.is_admin == 1) {
					// redirect to control panel for admin
					next();
				} else {
					// redirect to dashboard
				}
			} else {
				next();
			}
		}
	} else if (to.matched.some(record => record.meta.guest)) {
		if (store.state.auth == null) {
			// console.log('not logged in!');
			next();
		} else {
			// console.log('logged in!');
			next();
			// next({ name: 'userboard'})
		}
	}else {
		next();
	}
});


/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

new Vue({
	el: '#app',
	router,
	store
});
