import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

import VuexPersistence from 'vuex-persist';

const store = new Vuex.Store({
    state: {
        auth: null,
        token: null,
        user: {
            id: null,
            username: null,
            surname: null,
            photo: null,
            gender: null,
            is_admin: false,
            room_requested: false,
            has_room: false,
        },
        dorm: null,
        dormName: null,
        busy: false,
    },
    mutations: {
        auth(state, value) {
            state.auth = value;
        },
        token(state, value) {
            state.token = value;
            window.axios.defaults.headers.common['Authorization'] = 'Bearer '.concat(value);
        },
        user(state, user) {
            state.user.id = user.id;
            state.user.username = user.name;
            state.user.surname = user.surname;
            state.user.photo = user.photo;
            state.user.role_id = user.roles[0].id;
            state.user.gender = user.gender;
        },
        is_admin(state, value) {
            state.user.is_admin = value;
        },
        room_requested(state, value) {
            state.user.room_requested = value;
        },
        has_room(state, value) {
            state.user.has_room = value;
        },
        logout(state) {
            state.auth = null;
            state.user.id = null;
            state.user.username = null;
            state.user.surname = null;
            state.user.photo = null;
            state.user.role_id = null;
            state.user.is_admin = false;
            state.user.room_requested = false;
            state.user.has_room = false;
            state.user.gender = null;
            state.token = null;
            state.dorm = null;
            state.dormName = null;
            delete window.axios.defaults.headers.common['Authorization'];
        },
        dorm(state, value) {
            state.dorm = value;
        },

        dormName(state, value) {
            state.dormName = value;
        },

        busy(state, value) {
            state.busy = value;
        }
    },
    plugins: [new VuexPersistence().plugin]
});
if (store.state.token) {
    window.axios.defaults.headers.common['Authorization'] = 'Bearer '.concat(store.state.token);
}


export default store;
