import Vue from 'vue';
import VueRouter from 'vue-router';

//components
import Info from './components/pages/main/Info';
import GoogleRegister from './components/pages/auth/GoogleRegister';
import DormInfo from './components/pages/dorm-info/DormInfo';
import RoomInfo from './components/pages/room-info/RoomInfo';
import UserPanel from "./components/pages/user-panel/UserPanel";
import UserEdit from "./components/pages/user-edit/UserEdit";

//management components
import Home from "./management/pages/Home";
import Rooms from "./management/pages/Rooms";
import RoomCreate from "./management/pages/RoomCreate";
import Users from "./management/pages/Users";
import RoomRequests from "./management/pages/RoomRequests";
import Room from "./management/pages/Room";

//There are 2 router views - for main page and management page

let routes = [

    {
        path: '/',
        name: 'index',
        components: {
            'main': Info,
            'management': Home
        },
    },

    {
        path: '/rooms',
        name: 'Rooms',
        components: {
            'management': Rooms
        },
    },

    {
        path: '/room/create',
        name: 'RoomCreate',
        components: {
            'management': RoomCreate
        },
    },

    {
        path: '/room/view/:room_id',
        name: 'Room',
        components: {
            'management': Room
        },
    },

    {
        path: '/users',
        name: 'Users',
        components: {
            'management': Users
        },
    },

    {
        path: '/requests',
        name: 'RoomRequests',
        components: {
            'management': RoomRequests
        },
    },


    {
        path: '/dorm/:id',
        name: 'DormInfo',
        components: {
            'main': DormInfo
        },
        meta: {
            guest: true,
        },
    },

    {
        path: '/user',
        name: 'user',
        components: {
            'main': UserPanel
        },
        meta: {
            requiresAuth: true
        }
    },

    {
        path: '/user-edit',
        name: 'UserEdit',
        components: {
            'main': UserEdit
        },
        meta: {
            requiresAuth: true
        }
    },

    {
        path: '/dorm/:dorm_id/room/:room_id',
        name: 'RoomInfo',
        components: {
            'main': RoomInfo
        },
        meta: {
            guest: true,
        },
    },
    {
        path: '/googleRegister',
        name: 'googleRegister',
        components: {
            'main': GoogleRegister
        },
        meta: {
            guest: true,
        },
    },
];

Vue.use(VueRouter);

const router = new VueRouter({
    routes
});
export default router;
