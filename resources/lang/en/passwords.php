<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Passwords must be at least eight characters and match the confirmation.',
    'reset' => 'Your password has been reset!',
    'sent' => 'We have e-mailed your password reset link!',
    'token' => 'Unikalus kodas nebegalioja arba įvėdete neteisingą el-paštą, spustelkite mygtuka el-pašte iš naujo!',
    'user' => "Nerandame vartotojo su tokiu el-paštu, pasitikrinkite ar teisingas el-paštas",

];
