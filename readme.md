# Project Title

Bendrabučių paieškos, rezervavimo ir administravimo sistema Lietuvos švietimo įstaigoms.

## Built With

* [Laravel]
* [Vue]

### Prerequisites

Kad teisingai įsirašytų projektas reikėtu parsisiųsti šiuos package managerius:

* [Composer](https://getcomposer.org/download/)
* [Npm](https://www.npmjs.com/get-npm)

## Installing

Kad projektas teisingai veiktų, reikėtų paleisti šias komandas:

```
composer install
```


```
npm install
```


```
php artisan migrate:fresh --seed
```

## Requirements

* Auth
    + Register
    + Google
    + Login
* Admin panel
    + Basically godmode (gali pasiekti viską)
* Manager panel
    + CRUD students
    + CRUD dorm
    + CRUD rooms
* Student panel
    + View available dorm rooms
    + Request dorm room
* Landing page
    + Map
        - Available dorms nearby
        - Vacancy (laisvų vietų skaičius)
* Dorm page
    + Room list
* Room page
    + Basic info (nebūtina)
    + Photo/Photos (nebūtina)
    + Vacancy (laisvų vietų skaičius)
    + Roomates
    + Reserve (galbūt atskiras page paspaudus mygtuka reserve, arba kažkoks dropdown'as)
        - Calendar
    + Dorm room rating ??

## Roles-permission guide
*routes* galima bus pridėti kad tikrintu tiek rolę tiek permissioną, example:
```
Route::get('/admin/users', [
        'as' => 'admin.users',
        'uses' => 'Admin\AdminController@showUsers',
    ])->middleware(['Role:Administrator','Permission:user-list']);
```

*blade* example:

Ieško rolės:
```
@role('Administrator')
        <h1 class="font-semibold">Users Management</h1>
        <ul class="list-none mt-2">
            <a href="{{route('admin.users')}}" class="w-5/6"><li class="py-2 pl-2 flex justify-between">Vartotojai <a href="{{route('admin.user.create')}}" class="w-1/6"><i class="fas fa-plus"></i></a></li></a>
            <a href="{{route('admin.permissions')}}" class="w-5/6"><li class="py-2 pl-2 flex justify-between">Teisės <a href="{{route('admin.permissions.create')}}" class="w-1/6"><i class="fas fa-plus"></i></a></li></a>
        </ul>
@endrole
```
Ieško permissiono:
```
@can('user-edit')
    <!-- HTML-->
@elsecan('user-create')
    <!-- The HTML -->
@endcan

@cannot('user-edit')
    <!-- HTML -->
@elsecannot('user-create')
    <!-- HTML -->
@endcannot
```

Taip pat galime ir per *controller*, example:

Ieškom rolės:
```
if(User::hasRole('Administrator')){
      // Do something
}
```

Ieško permissiono
```
if(User::can('room-list`)){
      // Do something
};
```