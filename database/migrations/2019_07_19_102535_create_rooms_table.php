<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRoomsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rooms', function (Blueprint $table) {
            $table->bigIncrements('id')->unsigned()->index();
            $table->integer('room_number');
            $table->integer('floor');
            $table->integer('slots');
            $table->tinyInteger('room_gender');
            $table->text('description')->nullable();
            $table->bigInteger('dorm_id')->unsigned();
            $table->float('price', 4, 2);
            $table->text('photos');
            $table->timestamps();

            $table->foreign('dorm_id')->references('id')->on('dorms')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rooms');
    }
}
