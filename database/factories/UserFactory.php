<?php

/** @var Factory $factory */

use App\Models\User;
use Illuminate\Database\Eloquent\Factory;
use Illuminate\Support\Str;
use Faker\Generator as Faker;


$factory->define(User::class, function (Faker $faker) {
    return [
        'name' => $faker->firstName,
        'surname' => $faker->lastName,
        'phone' => '86'.mt_rand(1000000,9999999),
        'email_verified_at' => now(),
        'gender' => rand(1, 2),
        'email' => $faker->email,
        'password' => bcrypt('secret'),
        'university_id' => '1'
    ];
});
