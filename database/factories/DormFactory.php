<?php

/** @var Factory $factory */

use App\Models\Dorm;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;

$factory->define(Dorm::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'address' => $faker->address,
        'lat' => '55.7184307',
        'long' => '21.1161138',
        'university_id' => '1',
        'manager_id' => '2',
        'photos' => '[{"path":"\/images\/dorms\/default_dorm.png"}]',
    ];
});
