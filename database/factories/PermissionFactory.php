<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

$factory->define(\App\Models\Permission::class, function (Faker $faker) {
    return [
        'name' => Str::random(5),
        'description' => Str::random(10),
    ];
});
