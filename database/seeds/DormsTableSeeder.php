<?php

use Faker\Factory as Faker;
use Illuminate\Database\Seeder;

class DormsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        DB::table('dorms')->insert([
            [
                'id' => "1",
                'name' => 'Gulbių bendrabutis',
                'address' => 'Gulbių g. 8',
                'lat' => '55.7184307',
                'long' => '21.1161138',
                'university_id' => '1',
                'manager_id' => '2',
                'photos' => '[{"path":"\/images\/dorms\/default_dorm.png"}]',
            ],
            [
                "id" => "2",
                'name' => 'Taikos pr. bendrabutis',
                'address' => 'Taikos pr. 16',
                'lat' => '55.7022637',
                'long' => '21.1427848',
                'university_id' => '1',
                'manager_id' => '2',
                'photos' => '[{"path":"\/images\/dorms\/default_dorm.png"}]',
            ],

            [
                "id" => "3",
                'name' => 'Debreceno bendrabutis',
                'address' => 'Debreceno g. 25',
                'lat' => '55.6817956',
                'long' => '21.179516',
                'university_id' => '1',
                'manager_id' => '2',
                'photos' => '[{"path":"\/images\/dorms\/default_dorm.png"}]',
            ],

        ]);
    }
}
