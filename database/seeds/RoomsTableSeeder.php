<?php

use Illuminate\Database\Seeder;

use Faker\Factory as Faker;

class RoomsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        for($i = 1; $i <= 100; $i++) {
            $roomNumber = rand(100,500); // rand (100-500)
            DB::table('rooms')->insert([
                'id' => $i,
                'room_number' => $roomNumber,
                'floor' => substr($roomNumber, 0, -2), // Remove 2 digits from room_number
                'room_gender' => rand(1, 2),
                'slots' => rand(2,4),
                'dorm_id' => rand(1,3),
                'price' => rand(30, 70),
                'photos' => '[{"path":"\/images\/rooms\/9_1_5d3f6735884627.19832875.jpg"},{"path":"\/images\/rooms\/9_1_5d3f673589a070.51161328.jpg"},{"path":"\/images\/rooms\/9_1_5d3f67358b0419.09559739.jpg"}]'
            ]);
        }
    }
}
