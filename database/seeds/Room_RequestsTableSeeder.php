<?php

use Illuminate\Database\Seeder;

class Room_RequestsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('room_requests')->insert([
            [
                'user_id' => '9',
                'room_id' => '5'
            ],
            [
                'user_id' => '10',
                'room_id' => '6'
            ]
        ]);
    }
}
