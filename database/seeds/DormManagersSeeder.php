<?php

use Illuminate\Database\Seeder;

class DormManagersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('dorm_managers')->insert([
            [
                'user_id' => '2',
                'dorm_id' => '1',
            ],
            [
                'user_id' => '2',
                'dorm_id' => '2',
            ],
            [
                'user_id' => '2',
                'dorm_id' => '3',
            ],
        ]);
    }
}
