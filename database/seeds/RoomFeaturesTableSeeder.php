<?php

use Illuminate\Database\Seeder;

class RoomFeaturesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for($i = 1; $i <= 19; $i++) {
            DB::table('room_features')->insert([
                'room_id' => $i,
                'feature_id' => rand(1, 3),
            ]);
        }
    }
}
