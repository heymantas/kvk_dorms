<?php

use Illuminate\Database\Seeder;

use Faker\Factory as Faker;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'id' => "1",
            'name' => 'Test',
            'surname' => 'Admin',
            'phone' => '86'.mt_rand(1000000,9999999),
            'email' => 'admin@test.lt',
            'password' => bcrypt('secret'),
            'gender' => rand(1, 2),
            'university_id' => '1'
        ]);
        DB::table('users')->insert([
            "id" => "2",
            'name' => 'Test',
            'surname' => 'Mod',
            'phone' => '86'.mt_rand(1000000,9999999),
            'email' => 'mod@test.lt',
            'gender' => rand(1, 2),
            'password' => bcrypt('secret'),
            'university_id' => '1'
        ]);
        $faker = Faker::create();
        for($i = 3; $i<=10; $i++){
            DB::table('users')->insert([
                'id' => $i,
                'name' => $faker->firstName,
                'surname' => $faker->lastName,
                'phone' => '86'.mt_rand(1000000,9999999),
                'gender' => rand(1, 2),
                'email' => $faker->email,
                'password' => bcrypt('secret'),
                'university_id' => '1'
            ]);
        }
    }
}
