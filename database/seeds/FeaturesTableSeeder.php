<?php

use Illuminate\Database\Seeder;

class FeaturesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('features')->insert([
            [
                'name' => 'Nemokamas WiFi',
            ],
            [
                'name' => 'Skaitmeninė TV',
            ],
            [
                'name' => 'Atskiras dušas',
            ],
        ]);
    }
}
