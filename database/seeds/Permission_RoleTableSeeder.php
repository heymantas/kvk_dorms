<?php

use Illuminate\Database\Seeder;

class Permission_RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        foreach (range(1,30) as $index) { // Admin
            DB::table('permission_role')->insert([
                'role_id' => 1,
                'permission_id' => $index
            ]);
        }
        foreach (range(1,20) as $index) { //Manager
            if($index == 10) continue;
            DB::table('permission_role')->insert([
                'role_id' => 2,
                'permission_id' => $index
            ]);
        }
        DB::table('permission_role')->insert([
            [
                'role_id' => 3,
                'permission_id' => 6
            ],
            [
                'role_id' => 3,
                'permission_id' => 1
            ],
            [
                'role_id' => 3,
                'permission_id' => 2
            ],
            [
                'role_id' => 3,
                'permission_id' => 12
            ],
            [
                'role_id' => 3,
                'permission_id' => 13
            ],
            
        ]);
    }
}
