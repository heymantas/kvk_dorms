<?php

use Illuminate\Database\Seeder;

class UniversitiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('universities')->insert([
            [
                'name' => 'Klaipėdos valstybinė kolegija',
                'city_id' => '1'
            ],
        ]);
    }
}
