<?php

use Illuminate\Database\Seeder;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('permissions')->insert([
            [
                'name' => 'room-list',
                'description' => 'Kambarių sąrašas'
            ],
            [
                'name' => 'room-view',
                'description' => 'Kambarys'
            ],
            [
                'name' => 'room-create',
                'description' => 'Sukurti kambarį'
            ],
            [
                'name' => 'room-edit',
                'description' => 'Pakeisti kambarį'
            ],
            [
                'name' => 'room-delete',
                'description' => 'Ištrinti kambarį'
            ],
            [
                'name' => 'dorm-list',
                'description' => 'Bendrabučių sąrašas'
            ],
            [
                'name' => 'dorm-view',
                'description' => 'Bendrabutis'
            ],
            [
                'name' => 'dorm-create',
                'description' => 'Sukurti bendrabutį'
            ],
            [
                'name' => 'dorm-edit',
                'description' => 'Pakeisti bendrabutį'
            ],
            [
                'name' => 'dorm-delete',
                'description' => 'Ištrinti bendrabutį'
            ],
            [
                'name' => 'reserve-list',
                'description' => 'Rezervacijų sąrašas'
            ],
            [
                'name' => 'reserve-view',
                'description' => 'Rezervacija'
            ],
            [
                'name' => 'reserve-create',
                'description' => 'Sukurti rezervaciją'
            ],
            [
                'name' => 'reserve-edit',
                'description' => 'Pakeisti rezervaciją'
            ],
            [
                'name' => 'reserve-delete',
                'description' => 'Ištrinti rezervaciją'
            ],
            [
                'name' => 'student-list',
                'description' => 'Stundentų sąrašas'
            ],
            [
                'name' => 'student-view',
                'description' => 'Stundentas'
            ],
            [
                'name' => 'student-create',
                'description' => 'Sukurti stundentą'
            ],
            [
                'name' => 'student-edit',
                'description' => 'Pakeisti stundentą'
            ],
            [
                'name' => 'student-delete',
                'description' => 'Ištrinti stundentą'
            ],
            [
                'name' => 'university-list',
                'description' => 'Mokymosi įstaigos'
            ],
            [
                'name' => 'university-view',
                'description' => 'Mokymosi įstaigą'
            ],
            [
                'name' => 'university-create',
                'description' => 'Sukurti mokymosi įstaigą'
            ],
            [
                'name' => 'university-edit',
                'description' => 'Pakeisti mokymosi įstaigą'
            ],
            [
                'name' => 'university-delete',
                'description' => 'Ištrinti mokymosi įstaigą'
            ],
            [
                'name' => 'manager-list',
                'description' => 'Managers'
            ],
            [
                'name' => 'manager-view',
                'description' => 'Manager'
            ],
            [
                'name' => 'manager-create',
                'description' => 'Create manager'
            ],
            [
                'name' => 'manager-edit',
                'description' => 'Edit manager'
            ],
            [
                'name' => 'manager-delete',
                'description' => 'Delete manager'
            ]
        ]);
    }
}
