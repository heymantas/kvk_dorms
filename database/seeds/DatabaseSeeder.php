<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(FeaturesTableSeeder::class);
        $this->call(CitiesTableSeeder::class);
        $this->call(UniversitiesTableSeeder::class);
        $this->call(RolesTableSeeder::class);
        $this->call(UsersTableSeeder::class);
//        $this->call(PermissionsTableSeeder::class);
//        $this->call(Permission_RoleTableSeeder::class);
        $this->call(User_RolesTableSeeder::class);
        $this->call(DormsTableSeeder::class);
        $this->call(DormManagersSeeder::class);
        $this->call(RoomsTableSeeder::class);
        $this->call(RoomFeaturesTableSeeder::class);
        $this->call(Occupied_RoomsTableSeeder::class);
        $this->call(Room_RequestsTableSeeder::class);
    }
}
