<?php

use Illuminate\Database\Seeder;

class Occupied_RoomsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('occupied_rooms')->insert([
            [
                'user_id' => '6',
                'room_id' => '2',
                'paid_until' => "2019-12-01",
                'date_from' => '2019-09-15',
                'date_to' => '2020-09-01'
            ],
            [
                'user_id' => '7',
                'room_id' => '5',
                'paid_until' => "2020-01-01",
                'date_from' => '2019-09-15',
                'date_to' => '2020-09-01'
            ],
            [
                'user_id' => '8',
                'room_id' => '2',
                'paid_until' => "2020-01-01",
                'date_from' => '2019-09-15',
                'date_to' => '2020-09-01'
            ]
        ]);
    }
}
