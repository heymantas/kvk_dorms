<?php

use Illuminate\Database\Seeder;

class User_RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('role_user')->insert([
            [
                'user_id' => '1',
                'role_id' => '1'
            ],
            [
                'user_id' => '2',
                'role_id' => '2'
            ],
        ]);
        for($i = 3; $i<=10; $i++){
            DB::table('role_user')->insert([
                [
                    'user_id' => $i,
                    'role_id' => '3'
                ]
            ]);
        }
    }
}
